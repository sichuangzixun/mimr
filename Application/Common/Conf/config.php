<?php

return array(

    'DB_TYPE'              => 'mysql', // 数据库类型
    'DB_HOST'              => 'localhost', // 服务器地址
    'DB_NAME'              => 'yx_website_db', // 数据库名
    'DB_USER'              => 'tms-sql', // 用户名
    'DB_PWD'               => 'HyDrocGcANa7MKVR',  // 密码
    'DB_PORT'              => '3306', // 端口
    'DB_PREFIX'            => '', // 数据库表前缀
    'DB_CHARSET'           => 'utf8',      // 数据库编码默认采用utf8
    /*
   'DB_TYPE'   => 'mysql', // 数据库类型
   'DB_HOST'   => '124.173.135.2', // 服务器地址
   'DB_NAME'   => 'mimr_p4', // 数据库名
   'DB_USER'   => 'mimr_p4_f', // 用户名
   'DB_PWD'    => 'qsx621222',  // 密码
   'DB_PORT'   => '3306', // 端口
   'DB_PREFIX' => '', // 数据库表前缀
   */
    /* 调试配置 */
    'SHOW_PAGE_TRACE'      => true,

    /* URL配置 */
    'URL_CASE_INSENSITIVE' => true, //默认false 表示URL区分大小写 true则表示不区分大小写
    'URL_MODEL'            => 2, //URL模式
    'VAR_URL_PARAMS'       => '', // PATHINFO URL参数变量
    'URL_PATHINFO_DEPR'    => '/', //PATHINFO URL分割符   
    'URL_HTML_SUFFIX'      => 'html',

    //列表页每页显示数
    'PAGE_LIMIT_NUM'       => '15'

);