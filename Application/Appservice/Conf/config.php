<?php

define('UC_AUTH_KEY', '[1w=7Jou>~2tm&AR/PH6?pgr#f+-8N{qhLZS]!O%'); //加密KEY
 
define('ISLOCAL', preg_match('/(127\.0\.0\.1)|(192.168\.1\.\d)/i', $_SERVER['SERVER_ADDR']));
/*
// 服务器和本地有差异配置项
if (ISLOCAL) {
    //访问common接口的key
    define("COMMON_KEY","26bd951d3050f200d472bc6cec02c0e6");
    //访问common接口的url

    define("COMMON_API_URL","http://www.yx.com/index.php/api/interface/request");
}else{
    //访问common接口的key
    define("COMMON_KEY","26bd951d3050f200d472bc6cec02c0e6");
    //访问common接口的url

    define("COMMON_API_URL","http://interface.yx129.com/index.php/api/interface/request");
}
*/
/**
 * 后台配置文件
 * 所有除开系统级别的后台配置
 */
return array(
	//'配置项'=>'配置值'
	
	'USER_ADMINISTRATOR' => 1, //管理员用户ID
    'USER_ADMINISTRATOR_GROUP' => 4, //超级管理员组ID
	
	/* 数据库配置 
	'DB_TYPE'   => 'mysql', // 数据库类型
    'DB_HOST'   => 'localhost', // 服务器地址
    'DB_NAME'   => 'yx_website_db', // 数据库名
    'DB_USER'   => 'mimr_f', // 用户名
    'DB_PWD'    => 'qsx123456',  // 密码
    'DB_PORT'   => '3306', // 端口
    'DB_PREFIX' => '', // 数据库表前缀
	*/
	
	
	'__UPLOAD__' =>__ROOT__.'Public/',
	/* 模板相关配置 */
    'TMPL_PARSE_STRING' => array(
        '__STATIC__' => __ROOT__ . '/Public/static',
        '__ADDONS__' => __ROOT__ . '/Public/' . MODULE_NAME . '/Addons',
        '__IMG__'    => __ROOT__ . '/Public/' . MODULE_NAME . '/images',
        '__CSS__'    => __ROOT__ . '/Public/' . MODULE_NAME . '/css',
        '__JS__'     => __ROOT__ . '/Public/' . MODULE_NAME . '/js',
		'__WX_URL__' => 'http://m.mimr.ipanyu.cn/', // 微信认证地址
    ),
	
	
    
    /* 调试配置 */
    'SHOW_PAGE_TRACE' => false,
	
	 /* URL配置 */
//    'URL_CASE_INSENSITIVE' => true, //默认false 表示URL区分大小写 true则表示不区分大小写
    'URL_MODEL'            => 2, //URL模式
    'VAR_URL_PARAMS'       => '', // PATHINFO URL参数变量
    'URL_PATHINFO_DEPR'    => '/', //PATHINFO URL分割符   

    /* 全局过滤配置 */
    'DEFAULT_FILTER' => '', //全局过滤函数   
	


    /* 后台错误页面模板 */
    'TMPL_ACTION_ERROR'     =>  MODULE_PATH.'View/default/Public/error.html', // 默认错误跳转对应的模板文件
    'TMPL_ACTION_SUCCESS'   =>  MODULE_PATH.'View/default/Public/success.html', // 默认成功跳转对应的模板文件
    'TMPL_EXCEPTION_FILE'   =>  MODULE_PATH.'View/default/Public/exception.html',// 异常页面的模板文件
	
	//模版主题
    'DEFAULT_THEME'  	=> 	'default',
    'THEME_LIST'		=>	'default,color',
    //'TMPL_DETECT_THEME' => 	true, // 自动侦测模板主题
    
    //列表页每页显示数
    'PAGE_LIMIT_NUM'    =>  '10'
);








