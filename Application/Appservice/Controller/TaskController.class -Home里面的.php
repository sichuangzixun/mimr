<?php
namespace Home\Controller;
use Think\Controller;
class TaskController extends AdminController {
      public  function getAllID($parent_id=0)
		{
				$return=0;	
				$sql1=" select cat_id from ft_category Where  parent_id=$parent_id";  //第二层
				$sql2="  select cat_id from ft_category Where parent_id in($sql1)";  //第三层
				$sql3="  select cat_id from ft_category Where parent_id in($sql2)";  //第四层
				$return=D('ft_category')->query($sql3);  
				
				return $return;
		}
		
		public function index()
		 {
			 		$user_id=session('user_id');
					$start = strtotime(date("Y-m-d 00:00:00"));
					$end = strtotime(date("Y-m-d 23:59:59"));
					
					$old_start=mktime(0,0,0,date("m"),date("d")-3,date("y"));
					$old_end = mktime(23,59,59,date("m"),date("d")-2,date("y"));
					
					$Hour=intval(date("H"));

					/*
					if($Hour>=0&&$Hour<=7){   // 凌晨0时到6时,第二天的凌晨0时到6点有效，即第二天的6点还会在，这个任务		
							$te_sSql="and ((task_time>=$old_start and  task_time<=$old_end) or (task_time>=$start and  task_time<=$end))";
					}
					else
					{
							$te_sSql=" and (task_time>=$start and  task_time<=$end)";
					}*/
					//echo date('Y-m-d H:i:s',$end); exit;
					$te_sSql=" and (task_time>=$old_start and  task_time<=$end)";
					
					
					//echo date("Y-m-d H:i:s",$old_start)."<br>".date("Y-m-d H:i:s",$old_end);
					//exit;
					$sSql.=" user_id=".$user_id."  $te_sSql";  //UNIX_TIMESTAMP('2015-5-16')
					$list=M('ft_schedule')->where($sSql)->order('task_time desc')->select();  //排班表
					if($list) {
						foreach($list as &$key){
								
								$key['store_name']=M('ft_store')->where(' store_id='.$key['store_id'])->getField('store_name'); //得到店名
								$progress_sum=M('ft_form_field')->where(" request=1 ")->count();	

									$key['progress_result']=M('ft_form_field_value')
								->join(" ft_form_field on ft_form_field.field_id=ft_form_field_value.field_id")
								->where("ft_form_field_value.user_id=$user_id  and ft_form_field_value.task_id=".$key['id']." and ft_form_field.request=1 ")
								->count();  //已经上传的

								
								
								$key['progress']=round(($key['progress_result']/$progress_sum)*100);	
								$key['jia_progress']=$key['progress']-30;
								
													
								
								
								}
								$this->assign('_list', $list); 
					}	
					$this->display();
		 }
		 
		 
		 public function doing()
		 {
		 			$list=M('ft_category')->where(' parent_id=0')->order('sort_order ')->select();
					
					 if($list) {
						foreach($list as &$key){
							if($key['cat_id']){
								$key['cat_list'] = D("ft_category")->where(" parent_id=".$key['cat_id'])->order("sort_order ")->select();
								//===========================================
								foreach($key['cat_list'] as &$key2){
									$key['cat_list2'] = D("ft_category")->where(" parent_id=".$key2['cat_id'])->order("sort_order ")->select();
								}
								
							}
								
						}
							$this->assign('_list', $list); 
					 }
		 
					
					 $this->display();
		 }
		 
		 
		 public function doing2($cat_id)
		 {
		 			$list=M('ft_category')->where(' parent_id='.$cat_id)->order('sort_order ')->select();
					
					 if($list) {
						foreach($list as &$key){
							if($key['cat_id']){
$key['cat_list'] = D("ft_category")->where(" parent_id=".$key['cat_id'])->order("sort_order ")->select();
//$key['count']=D("ft_form_field_value")->where(" parent_id=".$key['cat_list']['cat_id'])->count()
							}
								
						}
							$this->assign('_list', $list); 
					 }
		 
					
					 $this->display();
		 }
		 
		 
		  public function history()
		
		{		
/*
$progress_sum=M('ft_form_field')->where(" request=1 ")->count();	

							    $progress_result=M('ft_form_field_value')
								->join(" ft_form_field on ft_form_field.field_id=ft_form_field_value.field_id")
								->where("ft_form_field_value.user_id=$user_id  and ft_form_field_value.task_id=".$schedule['id']." and ft_form_field.request=1 ")
								->count();  //已经上传的
						*/		
		 			$user_id=$_GET['user_id'];
					if(!isset($user_id))
					{
						$user_id=session('user_id');
					}
					$start = strtotime(date("Y-m-d 00:00:00"));
					$end = strtotime(date("Y-m-d 23:59:59"));
					$sSql=" user_id=".$user_id; 
					
					$list=M('ft_schedule')->where($sSql)->order('task_time asc')->select();  //排班表
					$progress_sum=M('ft_form_field')->where(" request=1 ")->count();		
					 if($list) {
						foreach($list as &$key){
							if($key['store_id']){
							$key['store_name'] = D("ft_store")->where(" store_id=".$key['store_id'])->getField('store_name');

//---------------------------进度
//$progress_result=M('ft_form_field_value')->where(" user_id=$user_id and task_id=".$key['id'])->count();

$progress_result=M('ft_form_field_value')
->join(" ft_form_field on ft_form_field.field_id=ft_form_field_value.field_id")
->where("ft_form_field_value.user_id=$user_id  and ft_form_field_value.task_id=".$key['id']." and ft_form_field.request=1 ")
->count();  //已经上传的
								
$progress=round(($progress_result/$progress_sum)*100);	
$key['progress']=$progress;
$key['jia_progress']=$progress-30;

//---------------------------进度
							}
								
						}
							$this->assign('_list', $list); 
					 }
					$this->display();
		 }
		 
		 
		   public function upload_img()
		 {				
		 					$data['code']=40009;
							$data['msg']="未处理";
							$data['data']=0;
							
							
							$base64=file_get_contents("php://input"); //获取输入流
							$base64=json_decode($base64,1);
							$img_data = $base64['base64'];
							preg_match("/data:image\/(.*);base64,/",$img_data,$res);
							$ext = $res[1];

							if(!in_array($ext,array("jpg","jpeg","png","gif"))){
										$data['code']=40001;
										$data['msg']="上传图片格式不正确";
										$data['data']=0;
							}
							$filename= C(__UPLOAD__).time().'.'.$ext;
							$img_data = preg_replace("/data:image\/(.*);base64,/","",$img_data);

							$field_id=$base64['field_id'];   //得到字的id
							$form_field=M('ft_form_field')->where(" field_id=".$field_id)->find();
							
							$parent_id=$form_field['parent_id'];
							$field_name=$form_field['field_name'];
							//新增加的
							$is_picture=$form_field['is_picture'];  //是否传文件名称可文件
							$is_more=$form_field['is_more'];   //是否多图片
							
							$img_old_name=$base64['img_old_name'];
							$task_id=$base64['task_id'];
							
							if (file_put_contents($filename,base64_decode($img_data))===false) {
									 $data['code']=40002;
									 $data['msg']="上传图片失败";
									 $data['data']=0;
							}else{
									if(!$is_picture){
										$filename='';
									}
								    $map=" task_id=$task_id and field_id=$field_id and user_id=".session('user_id');
									$get_count=M('ft_form_field_value')->where($map)->count();
									if(!$get_count)
									{
												$data0['field_name']=$field_name;
												$data0['parent_id']=$parent_id;
												$data0['task_id']=$task_id;
												$data0['field_value']=$filename;
												$data0['field_id']=$field_id;
												$data0['old_name']=$img_old_name;
												$data0['user_id']=session('user_id');
												$data0['create_time']=time();
												M('ft_form_field_value')->add($data0);
											
											    $data['code']=40000;
											    $data['msg']="上传成功";
											    $data['data']="<a onclick=del_task_photo('".$field_id."','".$task_id."');  id='del_task_photo".$data0['field_id']."'>".$data0['old_name']."   删除</a>   "; 
									}
									else
									{
											 if($is_more){    //如果支持多图片则修改文件名
														 $ft_form_field_value=M('ft_form_field_value')->where($map)->find();
														 $old_field_value=$ft_form_field_value['field_value'];
														 $old_old_name=$ft_form_field_value['old_name'];
											 
														 $data0['field_value']=$old_field_value."|".$filename;
														 $data0['old_name']=$old_old_name."|".$img_old_name;
														 $res=M('ft_form_field_value')->where($map)->save($data0);
													     if($res){
															 $data['code']=40000;
															 $data['msg']="上传成功222";
															 $data['data']="<a onclick=del_task_photo('".$field_id."','".$task_id."');  id='del_task_photo".$field_id."'>".$data0['old_name']."   删除</a>   "; 
														 }else{
															  $data['code']=40004;
															  $data['msg']="更新失败";
														 }
											 }else{
														 $data['code']=40003;
														 $data['msg']="有重复记录，不可以上传";
														 $data['data']='限制只能上传一张图片';
											 }
									}
									
							}
							echo json_encode($data);
							

		 				

														
														
							
		}			
		  public function upload()
		 {
if(IS_POST){
							$arrayPost=$_POST;//I("post.")
							//echo count($arrayPost)."测试";
							//得到图片字段名称						
							$tmp_name=M('ft_form_field')->where("field_type='file' and parent_id=".$arrayPost['cat_id'])->getField('field_name');
							$tmp_name_id=M('ft_form_field')->where("field_type='file' and parent_id=".$arrayPost['cat_id'])->getField('field_id');
							$img_data=$arrayPost['img_data'];
													
								foreach($arrayPost as $key=>$value)
								{
									//echo substr($key,0,5);
								  	if(substr($key,0,5)=="check")
									{
										$value=arrToStr($value);
									}
									else
									{
										$value=$value;
									}
									/*
									if(empty($value))
									{
											$this->error($key."=".$value."请完成本部分检查");
									}*/
									echo $key."=>".$value."\n";  //arrToStr
									
									if($key!="cat_id" and $key!="task_id" and $key!="count" and $key!="path")  //分类则不增加数据
									{
										    $task_id=$arrayPost['task_id'];
											$user_id=session('user_id');
										    $field_id=M('ft_form_field')->where("field_name='$key' and parent_id=".$arrayPost['cat_id'])->getField('field_id');
											$get_count=M('ft_form_field_value')->where(" task_id=$task_id and field_id=$field_id and user_id=$user_id")->count();
											if(!$get_count)
											{
													$data['field_name']=$key;
													$data['parent_id']=$arrayPost['cat_id'];
													$data['task_id']=$task_id;
													$data['field_value']=$value;
													$data['field_id']=$field_id;
													$data['create_time']=time();
													$data['user_id']=$user_id;
													M('ft_form_field_value')->add($data);
											}
											
									}
								}
								
								$count=$arrayPost['count'];
								$parent_id=M('ft_category')->where(" cat_id=".$arrayPost['cat_id'])->getField('parent_id');
								$task_id=$arrayPost['task_id'];
								if($count>1)
								 {
									 $this->redirect("upload/?task_id=$task_id&parent_id=$parent_id");
									 exit;
								 }
								 else
								 {
										 $this->redirect("doing/?task_id=$task_id#cat_id_".$parent_id);
										 //echo "<script>parent.location.reload();<//script>";
										 exit;
								 }
							
								
					
}
					else
					{     
							$parent_id=I("get.parent_id",0);
							$task_id=I("get.task_id",0);
							$subQuery=M('ft_category')->where(' parent_id='.$parent_id)->field('cat_id')->buildSql();  //得到所有的下级ID 202,203
							$allCount=M('ft_form_field')->where(' parent_id in '.$subQuery)->count('distinct(parent_id)');   //得到所有的下级ID的数量
							
							
$subCount=M('ft_form_field_value')->where("user_id=".session('user_id')." and task_id=$task_id and parent_id in ".$subQuery)->count('distinct(parent_id)');	 //已上传的数量				
$subQuery2=M('ft_form_field_value')->where("user_id=".session('user_id')." and task_id=$task_id and parent_id in ".$subQuery)->field('parent_id')->buildSql();   //得到已经上传过的
			//==============================================================================================	
				$list=M('ft_category')->where(' parent_id='.$parent_id)->field('cat_id')->select();		
		 		if($list) {
					$i=0;
					foreach($list as &$key){
						$count_total=M('ft_form_field')->where(' request=1 and parent_id='.$key['cat_id'])->count();  
						
						$count_sub=M('ft_form_field_value')
						->join(" ft_form_field on ft_form_field.field_id=ft_form_field_value.field_id")
						->where(" ft_form_field_value.user_id=".session('user_id')."  and ft_form_field_value.task_id=".$task_id." and ft_form_field.request=1 and  ft_form_field_value.parent_id=".$key['cat_id'])
						->count(); 
						if($count_total==$count_sub)
						{
							$all_cat_id.=$key['cat_id'].",";
							$i++;	
						}
					
					}	
				}
				$count=$i;	
				//echo $all_cat_id;exit;
				if($all_cat_id)
				{
					$all_cat_id=substr($all_cat_id,0,strlen($all_cat_id)-1);
					$cat_id=M('ft_category')->where("  parent_id=$parent_id and cat_id not in (".$all_cat_id.")")->order(' sort_order')->getField('cat_id');
				}
				else
				{
					$cat_id=M('ft_category')->where("  parent_id=$parent_id ")->order(' sort_order')->getField('cat_id');
				}
				//echo $all_cat_id."<br>".$cat_id;exit;
				//==============================================================================================					
							
					//$cat_id=M('ft_category')->where("  parent_id=$parent_id and cat_id not in ('".$all_cat_id."')")->order(' sort_order')->getField('cat_id');	
							$progress=round($subCount/$allCount,2)*100;
							$jia_pro=$progress-30;
						   // var_dump($cat_id);
							//exit;
							//$cat_id=I("get.cat_id",0);
							$list=M('ft_form_field')->where(' parent_id='.$cat_id)->order('sort_order desc')->select();
							$this->assign("_list",$list);
							$this->assign("cat_id",$cat_id);
							$this->assign("count",$allCount-$subCount);
							$this->assign("progress",$progress);
							$this->assign("jia_pro",$jia_pro);
							
							$third_parent_id=M('ft_category')->where(' cat_id='.$cat_id)->getField('parent_id');
							$four_parent_id=M('ft_category')->where(' cat_id='.$third_parent_id)->getField('parent_id');
							
							$this->assign("title",M('ft_category')->where(' parent_id='.$four_parent_id)->getField('cat_name'));
							$this->assign("title2",M('ft_category')->where(' cat_id='.$cat_id)->getField('cat_name'));
							$this->display();
					}

		 }
		 
		 
		  //问卷封面
		  public function start($task_id=0)
		 {
		 			if(IS_POST){
					        $task_id=$_POST['task_id']; 
							$schedule=M('ft_schedule')->where(" id=".$task_id)->find(); 
							$data['task_id']=$task_id;
							if(empty($_POST['address']))
							{
									$this->error("定位地址不能为空");
							}
							$data['store_id']=$schedule['store_id'];
							$data['user_id']=$schedule['user_id'];
							$data['check_date']=strtotime($schedule['builddate']);
							$data['create_time']=time();
							
							
							$data['check_start_time']=strtotime($_POST['check_start_time']);
							$data['check_end_time']=strtotime($_POST['check_end_time']);
							/*
							$data['store_manager']=$_POST['store_manager'];
							$data['store_manager_tel']=$_POST['store_manager_tel'];
							$data['jiedai']=$_POST['jiedai'];
							$data['jiedai_job']=$_POST['jiedai_job'];
							$data['jiedai_tel']=$_POST['jiedai_tel'];
							
							$data['is_car']=arrToStr($_POST['is_car']);
							$data['is_house']=arrToStr($_POST['is_house']);
							$data['is_tact']=arrToStr($_POST['is_tact']);
							$data['is_login']=arrToStr($_POST['is_login']);
							$data['is_other']=$_POST['is_other'];*/
							
							$data['address_true']=$_POST['address_true'];
							$data['address']=$_POST['address'];
							$count=M('ft_result')->where(" task_id=".$task_id)->count(); 
							if($count>0)
							{
									$res=M('ft_result')->where(" task_id=".$task_id)->save($data); 
							}
							else
							{
								   $res=M('ft_result')->add($data); 
							}
							
							if(0 < $res){
								 $this->redirect('Task/doing?task_id='.$task_id);
								 //$this->success('封面操作成功！',U('Task/doing?task_id='.$task_id));
							} else {
								$this->error($this->showRegError($res));
							}
					}
					else
					{
							$schedule=M('ft_schedule')->where(' id='.$task_id)->find();
							$result=M('ft_result')->where(' task_id='.$task_id)->find();
							$store=M('ft_store')->where(' store_id='.$schedule['store_id'])->find();
							
							$uname=M('ft_user')->where(' user_id='.$schedule['user_id'])->getField('uname');
							$this->assign('store', $store); 
							$this->assign('uname', $uname); 
							$this->assign('schedule', $schedule); 
							$this->assign('result', $result); 
							$this->display();
					}
		 }
		 
		 
		 
		   public function upload_end()
		 {
		 
		
								
							
								
								
								$task_id=I("get.task_id",0);
								
								$data['code']=40005;
							    $data['msg']="未知错误";
									
								$allcount=M('ft_form_field')->where(" request=1")->count();  
								
								$allcount_value=M('ft_form_field_value')
								->join(" ft_form_field on ft_form_field.field_id=ft_form_field_value.field_id")
								->where("ft_form_field_value.user_id=".session('user_id')."  and ft_form_field_value.task_id=$task_id and ft_form_field.request=1 ")
								->count();  //已经上传的
								
								
								//$allcount_value=M('ft_form_field_value')->where("task_id=$task_id and user_id=".session('user_id'))->count();  
								$cha_count=$allcount-$allcount_value;
								
								
								$sql=M('ft_form_field_value')->where("task_id=$task_id and user_id=".session('user_id'))->field('field_id')->buildSql();
								$row=M('ft_form_field')->where(' field_id not in '.$sql)->getField('field_id,field_title');  //得到所有的下级ID 202,203
								if($allcount!=$allcount_value)  //没完成
								{
										$data['code']=40002;
										$data['count']=$cha_count;
										$data['msg']=$row;
										
								}
								else
								{
										$data['code']=40000;
										$data['msg']="成功";
										$resutn['check_end_time']=time();
										M('ft_result')->where("task_id=$task_id and user_id=".session('user_id'))->save($resutn);
										
								}
								
								$count=M('ft_schedule')->where("id=$task_id and user_id=".session('user_id'))->count();    //没有这个任务
								if($count<=0)
								{
									$data['code']=40001;
									$data['msg']="此用户不存在此条任务";
								}
								echo json_encode($data);
				
		 }
		 
		 
		 
		//---------------------------------------------删除相片---------------------------------------------------
		public function del_task_photo(){
					$field_id=I("param.field_id",0);
					$user_id=session('user_id');
					$task_id=I("param.task_id",0);
					
					$map=" field_id=$field_id and task_id=$task_id and  user_id=".$user_id;			
					if(!$field_id){
						    $return['code']=40001;
							$return['msg']='参数有误';
							exit(json_encode($return));
						
					}
					$field_value=M('ft_form_field_value')->where($map)->getField('field_value');
					if(strpos($field_value,"|")){  //删除多张相片
							$arr=explode("|",$field_value);
							foreach($arr as $row){
								unlink($row);
							}
						
					}else{
							unlink($field_value);//删除单张相片
					}
					
					$res=M('ft_form_field_value')->where($map)->delete();
					if($res){
								$return['code']=40000;
								$return['msg']='成功';
								exit(json_encode($return));
					}else
					{
								$return['code']=40002;
								$return['msg']='删除表单内容失败'.strpos($field_value,"|");
								exit(json_encode($return));
					}
		}
		 
}