<?php
namespace Appservice\Controller;
use Think\Controller;
class TaskController extends Controller {
     
		
		public function index()
		 {			//session('user_id')

			 		$user_id=I("param.user_id",0);
					$start = strtotime(date("Y-m-d 00:00:00"));
					$end = strtotime(date("Y-m-d 23:59:59"));
					
					$old_start=mktime(0,0,0,date("m"),date("d")-3,date("y"));
					$old_end = mktime(23,59,59,date("m"),date("d")-2,date("y"));
					
					$Hour=intval(date("H"));

					
					$te_sSql=" and (task_time>=$old_start and  task_time<=$end)";
					
					
					
					$sSql =" user_id=".$user_id."  $te_sSql";
					$list=M('ft_schedule')->where($sSql)->order('task_time desc')->select();  //排班表

					if($list) {
						foreach($list as &$key){

									$key['store_name']=M('ft_store')->where(' store_id='.$key['store_id'])->getField('store_name'); //得到店名

									$subQuery=M('ft_category')->where('  is_jianchadian=1 and is_show=1 and project_id='.$key['project_id'])->field('cat_id')->buildSql();


									$progress_sum=M('ft_form_field')->where(" request=1 and parent_id in $subQuery ")->count();


									$key['progress_result']=M('ft_form_field_value')
									->join(" ft_form_field on ft_form_field.field_id=ft_form_field_value.field_id")
									->where("ft_form_field_value.user_id=$user_id  and ft_form_field_value.task_id=".$key['id']." and ft_form_field.request=1 ")
									->count();  //已经上传的

									$key['progress']=round(($key['progress_result']/$progress_sum)*100);	
									$key['jia_progress']=$key['progress']-30;

								}

								$return_data = array(
								'code'          =>  "40000",
								'msg'           =>  '读取成功',
								'data'           =>  $list,
								);
					}else{
								$return_data = array(
									'code'          =>  "40001",
									'msg'           =>  '暂无数据',
								);
					}	
					exit(json_encode($return_data));

		 }
		 
		 //问卷封面
		  public function start($task_id=0)
		 {
					$task_id=I("param.task_id",0);
					
		 			$schedule=M('ft_schedule')->where(' id='.$task_id)->find();
					if(!$schedule){
							$return_data = array(
								'code'          =>  "40001",
								'msg'           =>  '没有这条任务',
								);
							exit(json_encode($return_data));
					}
					$result=M('ft_result')->where(' task_id='.$task_id)->find();
					if($result['ok']){
							$return_data = array(
								'code'          =>  "40002",
								'msg'           =>  '这条任务已经完成啦',
								);
							exit(json_encode($return_data));
					}
					$store=M('ft_store')->where(' store_id='.$schedule['store_id'])->find();
					
					$uname=M('ft_user')->where(' user_id='.$schedule['user_id'])->getField('uname');
					$this->assign('store', $store); 
					$this->assign('uname', $uname); 
					$this->assign('schedule', $schedule); 
					$this->assign('result', $result); 
					
					if($schedule){
								$return_data = array(
								'code'          =>  "40000",
								'msg'           =>  '读取成功',
								'store'           =>  $store,  //店的相关信息
								'uname'           =>  $uname,	//任务人
								'schedule'           =>  $schedule,  //任务相关信息
								'result'           =>  $result,   //结果信息
								);
					}else{
								$return_data = array(
								'code'          =>  "40010",
								'msg'           =>  '读取失败',
								);
					}
					exit(json_encode($return_data));
		
		 }
		 
		 //保存封页数据
		 public function start_save(){
							$task_id=I("param.task_id",0); 
							$schedule=M('ft_schedule')->where(" id=".$task_id)->find(); 
							if(!$schedule){
									$return_data = array(
										'code'          =>  "40001",
										'msg'           =>  '没有这条任务',
										);
									exit(json_encode($return_data));
							}
					
							if(!I("param.address"))
							{
									$return_data = array(
										'code'          =>  "40002",
										'msg'           =>  '定位地址不能为空',
										);
									exit(json_encode($return_data));
							}
							$data['task_id']=$task_id;
							$data['store_id']=$schedule['store_id'];
							$data['user_id']=$schedule['user_id'];
							$data['check_date']=strtotime($schedule['builddate']);
							$data['project_id']=$schedule['project_id'];
							
							$data['check_start_time']=strtotime(I("param.check_start_time",0));
							$data['check_end_time']=strtotime(I("param.check_end_time",0));
							$data['address_true']=I("param.address_true",0);
							$data['address']=I("param.address",0);
							$count=M('ft_result')->where(" task_id=".$task_id)->count(); 
							if($count>0)
							{
									$data['update_time']=time();
									$res=M('ft_result')->where(" task_id=".$task_id)->save($data); 
							}
							else
							{
								   $data['create_time']=time();
								   $res=M('ft_result')->add($data); 
							}
							
							if(0 < $res){
									$return_data = array(
										'code'          =>  "40000",
										'msg'           =>  '封面操作成功！',
										);
									exit(json_encode($return_data));
							} else {
									$return_data = array(
										'code'          =>  "40010",
										'msg'           =>  '封面操作失败！'.$res,
										);
									exit(json_encode($return_data));
							}
		 }
		 
		 
		 //所有试题分类
		 public function doing()
		 {
					$task_id=I("param.task_id",0);
					$Task=M('ft_schedule')->where(" id=".$task_id)->find(); 
					if(!$Task){
							$return_data = array(
											'code'          =>  "40001",
											'msg'           =>  '没有此任务',
							);
							exit(json_encode($return_data));
					}
					$user_id=$Task['user_id'];
					$project_id=$Task['project_id'];
					
		 			$list=M('ft_category')->where(" parent_id=0 and  project_id=$project_id")->order('sort_order ')->select();
					if($list) {
								foreach($list as &$key){
									if($key['cat_id']){
										$key['data'] = D("ft_category")->where(" parent_id=".$key['cat_id'])->order("sort_order ")->select();
										
										//===========================================
										foreach($key['data'] as $k=>&$key2){
											$key['data'][$k]['data'] = D("ft_category")->where(" parent_id=".$key2['cat_id'])->order("sort_order ")->select();
											
											//--------------------------------------------------------------------------
											foreach($key['data'][$k]['data'] as $k3=>&$key3){
												
												$subQuery=M('ft_category')->where(' parent_id='.$key3['cat_id'])->field('cat_id')->buildSql();  //得到所有的下级ID 202,203
												$count_total=M('ft_form_field')->where(' request=1 and parent_id='.$key3['cat_id'])->count();  //全部必填写字段
												$count_sub=M('ft_form_field_value fffv')
												->join(" ft_form_field fff on fff.field_id=fffv.field_id")
												->where("fffv.user_id=".$user_id."  and fffv.task_id=".$task_id." and fff.request=1 and  fff.parent_id=".$key3['cat_id'])
												->count();  //已经上传的
												
												
												$count=$count_total-$count_sub;
												$progress=round($count_sub/$count_total,2)*100;
											
											
												$key['data'][$k]['data'][$k3]['progress']=$progress;
											}
											//--------------------------------------------------------------------------
										}
										
										
									}
										
								}
									
									
										$return_data = array(
												'code'          =>  "40000",
												'msg'           =>  '读取数据成功',
												'data'           =>  $list,
										);
					 }else{
		 
					
										$return_data = array(
												'code'          =>  "40010",
												'msg'           =>  '没有数据',
										);
									
					 }
					 exit(json_encode($return_data));
		 }
		 
		 //第一大类
		 public function doing1()
		 {
					$task_id=I("param.task_id",0,intval);
		 			$list=M('ft_category')->where(' parent_id=0')->order('sort_order desc')->select();
					if($list) {
										$return_data = array(
												'code'          =>  "40000",
												'msg'           =>  '读取数据成功',
												'data'           =>  $list,
										);
					 }else{
										$return_data = array(
												'code'          =>  "40010",
												'msg'           =>  '没有数据',
										);
					 }
					 exit(json_encode($return_data));
		 }
		 
		 public function doing_next()
		 {
					$cat_id=I("param.cat_id",0,intval);
		 			$list=M('ft_category')->where(' parent_id='.$cat_id)->order('sort_order desc')->select();
					if($list) {
										$return_data = array(
												'code'          =>  "40000",
												'msg'           =>  '读取数据成功',
												'data'           =>  $list,
										);
					 }else{
										$return_data = array(
												'code'          =>  "40010",
												'msg'           =>  '没有数据',
										);
					 }
					 exit(json_encode($return_data));
		 }
		 
		 
		  //得到试题
		  public function upload()
		 {
							$parent_id=I("param.parent_id",0,intval);
							$task_id=I("param.task_id",0,intval);
							if(!$task_id){
										$return_data = array(
												'code'=>  "40001",
												'msg'=>  '任务ID有问题',
										);
										exit(json_encode($return_data));
							}
							if($parent_id){
									//$subQuery=M('ft_category')->where(' parent_id='.$parent_id)->field('cat_id')->buildSql();  //得到所有的下级ID 202,203
									$sql=" parent_id=$parent_id";
							}else{
									$subQuery=M('ft_category cat')
									->join("ft_schedule task on task.project_id=cat.project_id")
									->where('  cat.is_jianchadian=1 and task.id='.$task_id)
									->field('cat_id')
									->buildSql(); 	
									$sql=" parent_id in $subQuery";
							}
						    //echo $subQuery;exit;
							$list=M('ft_form_field')->where($sql)->order(' parent_id,sort_order desc')->select();
							foreach($list as &$row){
								$parent_id_2=M("ft_category")->where(" cat_id=".$row['parent_id'])->getField("parent_id");
								$row['parent_id_2']=$parent_id_2;
							}
							if($list) {
										$return_data = array(
												'code'=>  "40000",
												'msg'=>  '读取成功',
												'data'=>  $list,
										);
							}else{
										$return_data = array(
												'code'=>  "40010",
												'msg'=>  '暂无数据',
										);
							}
							exit(json_encode($return_data));
							
							//$arr = json_decode($_POST['data'],true);
							//print_r($arr);
							
							//$arrayPost=$_POST;//I("post.")
							//echo count($arrayPost)."测试";
							//得到图片字段名称		
/*							
							$tmp_name=M('ft_form_field')->where("field_type='file' and parent_id=".$arrayPost['cat_id'])->getField('field_name');
							$tmp_name_id=M('ft_form_field')->where("field_type='file' and parent_id=".$arrayPost['cat_id'])->getField('field_id');
							$img_data=$arrayPost['img_data'];
													
								foreach($arrayPost as $key=>$value)
								{
									//echo substr($key,0,5);
								  	if(substr($key,0,5)=="check")
									{
										$value=arrToStr($value);
									}
									else
									{
										$value=$value;
									}
									/*
									if(empty($value))
									{
											$this->error($key."=".$value."请完成本部分检查");
									}*/
									//echo $key."=>".$value."\n";  //arrToStr
									/*
									if($key!="cat_id" and $key!="task_id" and $key!="count" and $key!="path")  //分类则不增加数据
									{
										    $task_id=$arrayPost['task_id'];
											$user_id=session('user_id');
										    $field_id=M('ft_form_field')->where("field_name='$key' and parent_id=".$arrayPost['cat_id'])->getField('field_id');
											$get_count=M('ft_form_field_value')->where(" task_id=$task_id and field_id=$field_id and user_id=$user_id")->count();
											if(!$get_count)
											{
													$data['field_name']=$key;
													$data['parent_id']=$arrayPost['cat_id'];
													$data['task_id']=$task_id;
													$data['field_value']=$value;
													$data['field_id']=$field_id;
													$data['create_time']=time();
													$data['user_id']=$user_id;
													M('ft_form_field_value')->add($data);
											}
											
									}
								}
								
								$count=$arrayPost['count'];
								$parent_id=M('ft_category')->where(" cat_id=".$arrayPost['cat_id'])->getField('parent_id');
								$task_id=$arrayPost['task_id'];
								if($count>1)
								 {
									 $this->redirect("upload/?task_id=$task_id&parent_id=$parent_id");
									 exit;
								 }
								 else
								 {
										 $this->redirect("doing/?task_id=$task_id#cat_id_".$parent_id);
										 //echo "<script>parent.location.reload();<//script>";
										 exit;
								 }
							*/

		 }
		 
		 
		 
		 
		 //我的所有历史任务
		 public function history(){	
					$user_id=I("param.user_id",0);
					if(!isset($user_id))
					{
						$user_id=session('user_id');
					}
					$start = strtotime(date("Y-m-d 00:00:00"));
					$end = strtotime(date("Y-m-d 23:59:59"));
					$sSql=" user_id=".$user_id; 
					$User=M("ft_user")->where(" user_id=$user_id")->find();
					if(!$User){
							$return_data = array(
								'code'          =>  "40001",
								'msg'           =>  '不存在此用户',
							);
							exit(json_encode($return_data));
					}
					$list=M('ft_schedule')->where($sSql)->order('task_time desc')->select();  //排班表
					if($list) {
						foreach($list as &$key){
							if($key['store_id']){
							$key['store_name'] = D("ft_store")->where(" store_id=".$key['store_id'])->getField('store_name');

								//---------------------------进度
								//$progress_result=M('ft_form_field_value')->where(" user_id=$user_id and task_id=".$key['id'])->count();
								$subQuery=M('ft_category')->where('  is_jianchadian=1 and is_show=1 and project_id='.$key['project_id'])->field('cat_id')->buildSql(); 	
								$progress_sum=M('ft_form_field')->where(" request=1 and parent_id in $subQuery")->count();
								
									
									
								$progress_result=M('ft_form_field_value')
								->join(" ft_form_field on ft_form_field.field_id=ft_form_field_value.field_id")
								->where("ft_form_field_value.user_id=$user_id  and ft_form_field_value.task_id=".$key['id']." and ft_form_field.request=1 ")
								->count();  //已经上传的
																
								$progress=round(($progress_result/$progress_sum)*100);	
								$key['progress']=$progress;
								//$key['jia_progress']=$progress-30;
								//---------------------------进度
							}
								
						}
							$return_data = array(
									'code'          =>  "40000",
									'msg'           =>  '读取数据成功',
									'data'           =>  $list,
							);
					 }else{
							$return_data = array(
								'code'          =>  "40010",
								'msg'           =>  '暂无数据',
							);
					 }
					
					exit(json_encode($return_data));
		 }
		 function test_get_progress($cat_id=0){
			 echo $this->get_progress($cat_id,148,501);
		 }
		 //得到分类的进度
		 function get_progress($cat_id=0,$user_id,$task_id){
					//$cat_id=374;
					$count_total=M('ft_form_field')->where(' request=1 and parent_id='.$cat_id)->count();  //全部必填写字段
					
					
					$count_sub=M('ft_form_field_value fffv')
					->join(" ft_form_field fff on fff.field_id=fffv.field_id")
					->where(" fff.request=1 and  fffv.parent_id=$cat_id and fffv.user_id=$user_id and fffv.task_id=$task_id")
					->count();  //已经上传的
					$i=$count_sub;
					$list=M('ft_form_field')->where(" field_type='file' and request=1 and parent_id=$cat_id")->select();  //全部必填写字段
					foreach($list as &$row){
							$count_limit=$row['count_limit'];
							$count_limit_true=M("ft_form_field_value")->where(" field_id=".$row['field_id'])->getField("field_value");
							$count_limit_true=explode("|",$count_limit_true);
							if((count($count_limit_true))!=$count_limit and ($count_limit!=0))
							{
								$i--;
								//echo count($count_limit_true)."=".$count_limit."<br>";
							}
					}
					
					
					
					
					
					$progress=round($i/$count_total,4)*100;	
					if($progress<0){
						$progress=0;
					}
					//echo "测试=".$progress;
					return $progress;
		 }
		 //保存用户提交的问卷答案内容，包括存json+post过来的数据
		  public function upload2(){		
					$result=I("param.result",0);
					$result=json_decode($result,true);
					
					if(!$result){
						$return_data = array(
								'code'          =>  "40001",
								'msg'           =>  '非json数据过来',
						);
						exit(json_encode($return_data));
					}
					$cat_id=$result['cat_id'];  	//分类ID
					$task_id=$result['task_id'];	//任务ID
					$Task=M("ft_schedule")->where(" id=$task_id")->find();
					$store_id=$Task['store_id'];
					$user_id=$Task['user_id'];
					$store_num=M("ft_store")->where(" store_id=$store_id")->getField("store_num");
					$data=$result['data'];
					
					
					$User=M('ft_user')->where(' user_id='.$user_id)->find();
					if(!$User){
							$return_data = array(
								'code'          =>  "40001",
								'msg'           =>  '没有这个用户',
							);
							exit(json_encode($return_data));
					}
					
					$schedule=M('ft_schedule')->where(' id='.$task_id)->find();
					if(!$schedule){
							$return_data = array(
								'code'          =>  "40002",
								'msg'           =>  '没有这条任务',
							);
							exit(json_encode($return_data));
					}
					foreach($data as &$row){
										$form_field=M("ft_form_field")->where(" field_id=".$row['field_id'])->find();
										if($form_field["field_type"]!='file'){
													$data2['field_value']=$row['field_value'];
													$data2['parent_id']=$cat_id;
													$data2['task_id']=$task_id;
													$data2['user_id']=$user_id;
													
													$data2['field_id']=$row['field_id'];
													$data2['field_name']=$form_field["field_name"];
													$data2['create_time']=time();
													M('ft_form_field_value')->add($data2);
										}
					}
					/*$count_total=M('ft_form_field')->where(' request=1 and parent_id='.$cat_id)->count();  //全部必填写字段
					$count_sub=M('ft_form_field_value fffv')
					->join(" ft_form_field fff on fff.field_id=fffv.field_id")
					->where("fffv.user_id=".$user_id."  and fffv.task_id=".$task_id." and fff.request=1 and  fff.parent_id=$cat_id")
					->count();  //已经上传的
					
					$progress=round($count_sub/$count_total,2)*100;		 */				
					$return_data = array(
								'code'          =>  "40000",
								'msg'           =>  '提交成功',
								'progress'           =>  $this->get_progress($cat_id,$user_id,$task_id),   //进度
								'其它'           =>  "user_id=$user_id,task_id=$task_id ,cat_id=$cat_id",  
								'parent_id'           =>  $cat_id,
					);
					exit(json_encode($return_data));
					/*
					$base_path = "./"; //接收文件目录  
					$target_path = $base_path . basename ( $_FILES ['field_id<9>file<0>'] ['name'] );  
					if (move_uploaded_file ( $_FILES ['field_id<9>file<0>'] ['tmp_name'], $target_path )) {  
						$array = array ("code" => "1", "message" => $_FILES ['field_id<9>file<0>'] ['name'] ,"result" => $_POST ['result'] );  
						echo json_encode ( $array );  
					} else {  
						$array = array ("code" => "0", "message" => "得不到图片" . $_FILES ['field_id<9>file<0>'] ['error'] );  
						echo json_encode ( $array );  
					}  

					$result=json_decode($result);
					var_dump($data);
					echo $result."<br>";
					*/
					
		 }
		 
		 //单独保存图片
		  public function upload3()
		 {
					$result=I("param.result",0);
					$result=json_decode($result,true);
					
					if(!$result){
						$return_data = array(
								'code'          =>  "40001",
								'msg'           =>  '非json数据过来',
						);
						exit(json_encode($return_data));
					}
					$cat_id=$result['cat_id'];  	//分类ID
					$task_id=$result['task_id'];	//任务ID
					$field_id=$result['field_id'];  	//试题ID
					$old_url=$result['old_url'];
					$field_type=M("ft_form_field")->where(" field_id=$field_id")->getField('field_type');
					if($field_type!='file'){
							$return_data = array(
								'code'          =>  "40001",
								'msg'           =>  '传过来的非图片的file',
							);
							exit(json_encode($return_data));
						
					}
					$result_user_id=$result['user_id'];
					$Store=M("ft_schedule")->where(" id=$task_id")->find();
					$store_id=$Store['store_id'];
					$user_id=$Store['user_id'];
					if($result_user_id!=$user_id){
							$return_data = array(
								'code'          =>  "40002",
								'msg'           =>  '传过来的user_id和实际任务的user_id不对应',
							);
							exit(json_encode($return_data));
					}
					$store_num=M("ft_store")->where(" store_id=$store_id")->getField("store_num");
					$data=$result['data'];
					
					
					$User=M('ft_user')->where(' user_id='.$user_id)->find();
					if(!$User){
							$return_data = array(
								'code'          =>  "40003",
								'msg'           =>  '没有这个用户',
							);
							exit(json_encode($return_data));
					}
					
					$schedule=M('ft_schedule')->where(' id='.$task_id)->find();
					if(!$schedule){
							$return_data = array(
								'code'          =>  "40004",
								'msg'           =>  '没有这条任务',
							);
							exit(json_encode($return_data));
					}
					$project_id=M("ft_form_field")->where(" field_id=".$field_id)->getField("project_id");
	
					
						$key="file";
						$base_path = "upload/project_id_$project_id/user_id_$user_id/task_id_".$task_id."_store_num_".$store_num."/"; //接收文件目录 
						if (!is_dir($base_path)){  
							mkdir(iconv("UTF-8", "GBK", $base_path),0777,true); 
						}
						$target_path = $base_path . basename ( $_FILES [$key] ['name'] ).".jpg";  
						if (move_uploaded_file ( $_FILES [$key] ['tmp_name'], $target_path )) {  
									$array = array ("code" => "40000", "msg" => $_FILES [$key] ['name'] ,"result" => $_POST ['result'] );  
							
									$sql=" parent_id=$cat_id and user_id=$user_id and field_id=$field_id and task_id=$task_id";
									$FFFV=M('ft_form_field_value')->where($sql)->find();
									if($FFFV){
												$data2['field_value']=$FFFV['field_value']."|"."/".$target_path;
												$data2['old_name']=$FFFV['old_name']."|".$old_url;
												$data2['create_time']=time();
												M('ft_form_field_value')->where($sql)->save($data2);
									}else{
												$data2['field_value']="/".$target_path;
												$data2['old_name']=$old_url;
												$data2['create_time']=time();
												$data2['parent_id']=$cat_id;
												$data2['task_id']=$task_id;
												$data2['user_id']=$user_id;
												$data2['field_id']=$field_id;
												$data2['field_name']=M("ft_form_field")->where(" field_id=".$field_id)->getField("field_name");
												M('ft_form_field_value')->add($data2);
									}
									
													
					
									$return_data = array(
												'code'          =>  "40000",
												'msg'           =>  '提交成功3',
												'progress'           =>  $this->get_progress($cat_id,$user_id,$task_id),  //进度
												'parent_id'           =>  $cat_id,
									);
									exit(json_encode($return_data));
					
					
						} else {  
									$array = array ("code" => "40005", "msg" => "得不到图片" . $_FILES [$key] ['error'] ); 
									$return_data = array(
												'code'          =>  "40010",
												'msg'           =>  '得不到图片',
									);
									exit(json_encode($return_data));
						}  
						
						
					
		 }
		 
		 
		 //提交完成任务
		    public function upload_end()
		 {
								$user_id=I("param.user_id",0,intval);
								$task_id=I("get.task_id",0,intval);
								$User=M("ft_user")->where(" user_id=$user_id")->find();
								if(!$User){
										$return_data = array(
											'code'          =>  "40001",
											'msg'           =>  '不存在此用户',
										);
										exit(json_encode($return_data));
								}
								 
								$schedule=M('ft_schedule')->where("user_id=$user_id and id=$task_id")->find();
								if(!$schedule){
										$return_data = array(
											'code'          =>  "40002",
											'msg'           =>  '此用户不存在此条任务',
										);
										exit(json_encode($return_data));
								}
					
			
								
								$allcount=M('ft_form_field')->where(" request=1")->count();  

								$allcount_value=M('ft_form_field_value')
								->join(" ft_form_field on ft_form_field.field_id=ft_form_field_value.field_id")
								->where("ft_form_field_value.user_id=".$user_id."  and ft_form_field_value.task_id=$task_id and ft_form_field.request=1 ")
								->count();  //已经上传的
								
							
								$cha_count=$allcount-$allcount_value;
								
								
								$sql=M('ft_form_field_value')->where("task_id=$task_id and user_id=".$user_id)->field('field_id')->buildSql();
								$row=M('ft_form_field')->where(' field_id not in '.$sql)->getField('field_id,field_title');  //得到所有的下级ID 202,203
								if($allcount!=$allcount_value)  //没完成
								{
										$data['code']=40010;
										$data['msg']="还差$cha_count条数据没完成,数据有$row";
										$data['count']=$cha_count;
								}
								else
								{
										$data['code']=40000;
										$data['msg']="提交成功";
										$resutn['check_end_time']=time();
										M('ft_result')->where("task_id=$task_id and user_id=".$user_id)->save($resutn);
										
								}
								
								
								echo json_encode($data);
				
		 }
		 
		 //浏览任务信息
		    public function view(){
					$task_id=I("param.task_id",0);
					$Task=M("ft_schedule")->where(" id=$task_id")->find();
					if(!$Task){
							$this->error("没有此任务信息");
					}
					$user_id=$Task['user_id'];
					$nickname=M("ft_user")->where(" user_id=$user_id")->getField("uname");

					$project_id=$Task['project_id'];
					$project_name=M("ft_project")->where(" id=$project_id")->getField("name");
					
					$store_id=$Task['store_id'];
					$Store=M("ft_store")->where(" store_id=$store_id")->find();
					$store_name=$Store["store_name"];
					$store_address=$Store["store_address"];
					
					$Task['nickname']=$nickname;
					$Task['project_name']=$project_name;
					$Task['store_name']=$store_name;
					$Task['store_address']=$store_address;
					
					$this->assign("info",$Task);
					$this->display();
			}
}