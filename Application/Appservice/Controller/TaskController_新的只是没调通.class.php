<?php
namespace Appservice\Controller;
use Think\Controller;
class TaskController extends Controller {
     
		
		public function index()
		 {			//session('user_id')
			 		$user_id=I("param.user_id",0);
					$start = strtotime(date("Y-m-d 00:00:00"));
					$end = strtotime(date("Y-m-d 23:59:59"));
					
					$old_start=mktime(0,0,0,date("m"),date("d")-3,date("y"));
					$old_end = mktime(23,59,59,date("m"),date("d")-2,date("y"));
					
					$Hour=intval(date("H"));

					
					$te_sSql=" and (task_time>=$old_start and  task_time<=$end)";
					
					
					
					$sSql.=" user_id=".$user_id."  $te_sSql";  
					$list=M('ft_schedule')->where($sSql)->order('task_time desc')->select();  //排班表
					if($list) {
						foreach($list as &$key){
									$key['store_name']=M('ft_store')->where(' store_id='.$key['store_id'])->getField('store_name'); //得到店名
									$progress_sum=M('ft_form_field')->where(" request=1 ")->count();	

									$key['progress_result']=M('ft_form_field_value')
									->join(" ft_form_field on ft_form_field.field_id=ft_form_field_value.field_id")
									->where("ft_form_field_value.user_id=$user_id  and ft_form_field_value.task_id=".$key['id']." and ft_form_field.request=1 ")
									->count();  //已经上传的

									$key['progress']=round(($key['progress_result']/$progress_sum)*100);	
									$key['jia_progress']=$key['progress']-30;
								}
								$return_data = array(
								'code'          =>  "40000",
								'msg'           =>  '读取成功',
								'data'           =>  $list,
								);
					}else{
								$return_data = array(
									'code'          =>  "40001",
									'msg'           =>  '暂无数据',
								);
					}	
					exit(json_encode($return_data));
					
		 }
		 
		 //问卷封面
		  public function start($task_id=0)
		 {
					$task_id=I("param.task_id",0);
					
		 			$schedule=M('ft_schedule')->where(' id='.$task_id)->find();
					if(!$schedule){
							$return_data = array(
								'code'          =>  "40001",
								'msg'           =>  '没有这条任务',
								);
							exit(json_encode($return_data));
					}
					$result=M('ft_result')->where(' task_id='.$task_id)->find();
					if($result['ok']){
							$return_data = array(
								'code'          =>  "40002",
								'msg'           =>  '这条任务已经完成啦',
								);
							exit(json_encode($return_data));
					}
					$store=M('ft_store')->where(' store_id='.$schedule['store_id'])->find();
					
					$uname=M('ft_user')->where(' user_id='.$schedule['user_id'])->getField('uname');
					$this->assign('store', $store); 
					$this->assign('uname', $uname); 
					$this->assign('schedule', $schedule); 
					$this->assign('result', $result); 
					
					if($schedule){
								$return_data = array(
								'code'          =>  "40000",
								'msg'           =>  '读取成功',
								'store'           =>  $store,  //店的相关信息
								'uname'           =>  $uname,	//任务人
								'schedule'           =>  $schedule,  //任务相关信息
								'result'           =>  $result,   //结果信息
								);
					}else{
								$return_data = array(
								'code'          =>  "40010",
								'msg'           =>  '读取失败',
								);
					}
					exit(json_encode($return_data));
		
		 }
		 
		 //保存封页数据
		 public function start_save(){
							
							$task_id=I("param.task_id",0); 
							$schedule=M('ft_schedule')->where(" id=".$task_id)->find(); 
							$data['task_id']=$task_id;
							if(!I("param.address"))
							{
									$return_data = array(
										'code'          =>  "40001",
										'msg'           =>  '定位地址不能为空',
										);
									exit(json_encode($return_data));
							}
							$data['store_id']=$schedule['store_id'];
							$data['user_id']=$schedule['user_id'];
							$data['check_date']=strtotime($schedule['builddate']);
							$data['create_time']=time();
							
							$data['check_start_time']=strtotime(I("param.task_id",0));
							$data['check_end_time']=strtotime(I("param.task_id",0));
							
							
							$data['address_true']=I("param.task_id",0);
							$data['address']=I("param.task_id",0);
							$count=M('ft_result')->where(" task_id=".$task_id)->count(); 
							if($count>0)
							{
									$res=M('ft_result')->where(" task_id=".$task_id)->save($data); 
							}
							else
							{
								   $res=M('ft_result')->add($data); 
							}
							
							if(0 < $res){
									$return_data = array(
										'code'          =>  "40000",
										'msg'           =>  '封面操作成功！',
										);
									exit(json_encode($return_data));
							} else {
									$return_data = array(
										'code'          =>  "40010",
										'msg'           =>  '封面操作失败！'.$res,
										);
									exit(json_encode($return_data));
							}
		 }
		 
		 
		 
		 public function doing()
		 {
					$task_id=I("param.task_id",0);
		 			$list=M('ft_category')->where(' parent_id=0')->order('sort_order ')->select();
					if($list) {
								foreach($list as &$key){
									if($key['cat_id']){
										$key['data'] = D("ft_category")->where(" parent_id=".$key['cat_id'])->order("sort_order ")->select();
										
										//===========================================
										foreach($key['data'] as $k=>&$key2){
											$key['data'][$k]['data'] = D("ft_category")->where(" parent_id=".$key2['cat_id'])->order("sort_order ")->select();
										}
										
										
									}
										
								}
									
									
										$return_data = array(
												'code'          =>  "40000",
												'msg'           =>  '读取数据成功',
												'data'           =>  $list,
										);
					 }else{
		 
					
										$return_data = array(
												'code'          =>  "40010",
												'msg'           =>  '没有数据',
										);
									
					 }
					 exit(json_encode($return_data));
		 }
		 
		 //得到试题
		  //得到试题
		  public function upload()
		 {
   
							$parent_id=I("get.parent_id",0);
							$task_id=I("get.task_id",0);
							$subQuery=M('ft_category')->where(' parent_id='.$parent_id)->field('cat_id')->buildSql();  //得到所有的下级ID 202,203
							
							
							$list=M('ft_form_field')->where(' parent_id in'.$subQuery)->order('sort_order desc')->select();
							
							if($list) {
										$return_data = array(
												'code'=>  "40000",
												'msg'=>  '读取成功',
												'data'=>  $list,
										);
							}else{
										$return_data = array(
												'code'=>  "40010",
												'msg'=>  '暂无数据',
										);
							}
							exit(json_encode($return_data));
							
							//$arr = json_decode($_POST['data'],true);
							//print_r($arr);
							
							$arrayPost=$_POST;//I("post.")
							//echo count($arrayPost)."测试";
							//得到图片字段名称						
							$tmp_name=M('ft_form_field')->where("field_type='file' and parent_id=".$arrayPost['cat_id'])->getField('field_name');
							$tmp_name_id=M('ft_form_field')->where("field_type='file' and parent_id=".$arrayPost['cat_id'])->getField('field_id');
							$img_data=$arrayPost['img_data'];
													
								foreach($arrayPost as $key=>$value)
								{
									//echo substr($key,0,5);
								  	if(substr($key,0,5)=="check")
									{
										$value=arrToStr($value);
									}
									else
									{
										$value=$value;
									}
									/*
									if(empty($value))
									{
											$this->error($key."=".$value."请完成本部分检查");
									}*/
									echo $key."=>".$value."\n";  //arrToStr
									
									if($key!="cat_id" and $key!="task_id" and $key!="count" and $key!="path")  //分类则不增加数据
									{
										    $task_id=$arrayPost['task_id'];
											$user_id=session('user_id');
										    $field_id=M('ft_form_field')->where("field_name='$key' and parent_id=".$arrayPost['cat_id'])->getField('field_id');
											$get_count=M('ft_form_field_value')->where(" task_id=$task_id and field_id=$field_id and user_id=$user_id")->count();
											if(!$get_count)
											{
													$data['field_name']=$key;
													$data['parent_id']=$arrayPost['cat_id'];
													$data['task_id']=$task_id;
													$data['field_value']=$value;
													$data['field_id']=$field_id;
													$data['create_time']=time();
													$data['user_id']=$user_id;
													M('ft_form_field_value')->add($data);
											}
											
									}
								}
								
								$count=$arrayPost['count'];
								$parent_id=M('ft_category')->where(" cat_id=".$arrayPost['cat_id'])->getField('parent_id');
								$task_id=$arrayPost['task_id'];
								if($count>1)
								 {
									 $this->redirect("upload/?task_id=$task_id&parent_id=$parent_id");
									 exit;
								 }
								 else
								 {
										 $this->redirect("doing/?task_id=$task_id#cat_id_".$parent_id);
										 //echo "<script>parent.location.reload();<//script>";
										 exit;
								 }
							

		 }
		 
		 
		  public function upload2()
		 {
					$arrayPost=$_POST;
					$result=I("param.result",0);
					$result=json_decode($result);
					if(!$result){
						$return_data = array(
								'code'          =>  "40001",
								'msg'           =>  '非json数据过来',
						);
						exit(json_encode($return_data));
					}
					$cat_id=$result['cat_id'];  	//分类ID
					$task_id=$result['task_id'];	//任务ID
					$user_id=$result['user_id'];
					$data=$result['data'];
					var_dump($data);
					$User=M('ft_user')->where(' user_id='.$user_id)->find();
					if(!$User){
							$return_data = array(
								'code'          =>  "40001",
								'msg'           =>  '没有这个用户',
							);
							exit(json_encode($return_data));
					}
					
					$schedule=M('ft_schedule')->where(' id='.$task_id)->find();
					if(!$schedule){
							$return_data = array(
								'code'          =>  "40002",
								'msg'           =>  '没有这条任务',
							);
							exit(json_encode($return_data));
					}
					
					foreach($data as &$row){
										if($row['field_type']=="file"){
												$base_path = "./"; //接收文件目录  
												$target_path = $base_path . basename ( $_FILES ['field_id<9>file<0>'] ['name'] );  
												if (move_uploaded_file ( $_FILES ['field_id<9>file<0>'] ['tmp_name'], $target_path )) {  
													$array = array ("code" => "1", "message" => $_FILES ['field_id<9>file<0>'] ['name'] ,"result" => $_POST ['result'] );  
													echo json_encode ( $array );  
												} else {  
													$array = array ("code" => "0", "message" => "得不到图片" . $_FILES ['field_id<9>file<0>'] ['error'] );  
													echo json_encode ( $array );  
												}  
												
												$data2['field_value']=$row['field_value']['key'];
												$data2['old_name']=$row['field_value']['old_name'];
										}else{
												$data2['field_value']=$row['field_value'];
										}
										$data2['parent_id']=$cat_id;
										$data2['task_id']=$task_id;
										$data2['user_id']=$user_id;
										
										
										//$data['field_name']=$key;
										
										$data2['field_id']=$row['field_id'];
										$data2['create_time']=time();
										M('ft_form_field_value')->add($data2);
					}
					var_dump($data);
					$return_data = array(
								'code'          =>  "40000",
								'msg'           =>  '提交成功',
					);
					exit(json_encode($return_data));
		 }
		 
		 //我的所有历史任务
		 public function history(){	
					$user_id=I("param.user_id",0);
					if(!isset($user_id))
					{
						$user_id=session('user_id');
					}
					$start = strtotime(date("Y-m-d 00:00:00"));
					$end = strtotime(date("Y-m-d 23:59:59"));
					$sSql=" user_id=".$user_id; 
					$User=M("ft_user")->where(" user_id=$user_id")->find();
					if(!$User){
							$return_data = array(
								'code'          =>  "40001",
								'msg'           =>  '不存在此用户',
							);
							exit(json_encode($return_data));
					}
					$list=M('ft_schedule')->where($sSql)->order('task_time asc')->select();  //排班表
					$progress_sum=M('ft_form_field')->where(" request=1 ")->count();		
					 if($list) {
						foreach($list as &$key){
							if($key['store_id']){
							$key['store_name'] = D("ft_store")->where(" store_id=".$key['store_id'])->getField('store_name');

								//---------------------------进度
								//$progress_result=M('ft_form_field_value')->where(" user_id=$user_id and task_id=".$key['id'])->count();
								$progress_result=M('ft_form_field_value')
								->join(" ft_form_field on ft_form_field.field_id=ft_form_field_value.field_id")
								->where("ft_form_field_value.user_id=$user_id  and ft_form_field_value.task_id=".$key['id']." and ft_form_field.request=1 ")
								->count();  //已经上传的
																
								$progress=round(($progress_result/$progress_sum)*100);	
								$key['progress']=$progress;
								$key['jia_progress']=$progress-30;
								//---------------------------进度
							}
								
						}
							$return_data = array(
									'code'          =>  "40000",
									'msg'           =>  '读取数据成功',
									'data'           =>  $list,
							);
					 }else{
							$return_data = array(
								'code'          =>  "40010",
								'msg'           =>  '暂无数据',
							);
					 }
					
					exit(json_encode($return_data));
		 }
}