<?php
namespace Appservice\Controller;
use Think\Controller;
class PublicController extends Controller {

  public function autoLogin($openid = null){
  	     $openid=$_GET['openid'];
		 if(empty($openid))
		 {
		      jsAlert('openid不能为空');
			  exit;	 
		 }
		 $map="  openid='".$openid."'";
		 $count = D('ft_user')->where($map)->count();
		 if($count>0){ // 登录成功，$uid 为登录的 UID
				$Member = D('ft_user')->where($map)->find();
				
				session('user_id',  $Member['user_id']);
				session('user_name',  $Member['user_name']);
				session('user_type',  $Member['intType']);
				$this->redirect('/Home/Index');
		  } else { //登录失败
				//$this->redirect(__WX_URL__."get_userinfo.php");
		 }
  
  }
 public function login(){
			$user_name=I("param.user_name");
            $password=I("param.password");
            if(!$user_name || !$password)
			{
					$return_data = array(
						'code'          =>  "40001",
						'msg'           =>  '账号和密码不能为空',
					);
					exit(json_encode($return_data));
			}

			$map['user_name'] = $user_name;
			$map['password'] = md5($password);
            $Member = D('ft_user')->where($map)->find();
			if($Member){ 
               		$return_data = array(
						'code'          =>  "40000",
						'msg'           =>  '登录成功',
						'data'           =>  $Member,
					);
					
            } else { 
               	    $return_data = array(
						'code'          =>  "40010",
						'msg'           =>  '登录失败',
					);
            }
			exit(json_encode($return_data));
        }

	function checkPhone($phone){
		$phone = trim($phone);
		if(preg_match("/1[3458]{1}\d{9}$/",$phone)){
			return true;
		}else{
			return false;
		}
	}
		//========================注册=======================
	public function reg(){
			$user_name = I("param.user_name");
			$name = I("param.name");   
            $password = I("param.password");
			if(!$user_name)
			{
				$return['code']=40001;
				$return['msg']='注册账号不能为空';
				exit(json_encode($return));
			}
			if(!$this->checkPhone($user_name))
			{
				$return['code']=40002;
				$return['msg']='手机号码不正确';
				exit(json_encode($return));
			}
			if(!$password)
			{
				$return['code']=40003;
				$return['msg']='登录密码不能为空';
				exit(json_encode($return));
			}
			if(!$name)
			{
				$return['code']=40004;
				$return['msg']='名字不能为空';
				exit(json_encode($return));
			}
			//判断是否已经添加了
			$result=M("ft_user")->where(" user_name='$user_name'")->count();
    		if($result){
    			$return['code']=40005;
				$return['msg']=$user_name.'手机号码已被注册，请确认重新输入！';
				exit(json_encode($return));
			}
				
		
		//增加会员
			$data['user_name']=$user_name;
			$data['password']=md5($password);
			$data['uName']=$name;
			$data['reg_time']=time();
			$data['last_time']=time();
			$user_id=M("ft_user")->add($data);
		//增加任务
			$data2['builddate']=date("Y-m-d",time());
			$data2['task']="新用户注册任务";
			$data2['user_id']=$user_id;
			$data2['store_id']=1595;
			$data2['task_time']=strtotime($data2['builddate']);
			$data2['create_time']=time();
			$data2['project_id']=10;
			$res2=M("ft_schedule")->add($data2);
			
			if($user_id and $res2){
				$return['code']=40000;
				$return['msg']='注册成功!';
				exit(json_encode($return));
			}else{
				$return['code']=40010;
				$return['msg']='注册失败!';
				exit(json_encode($return));
			}
			
	} 
	
	
   //退出登录 ,清除 session    ZSP 20150507
    public function logout() {
		$user_id=I("param.user_id",0,intval);
		$User=M("ft_user")->where(" user_id=$user_id")->find();
		if(!$User){
				$return_data = array(
					'code'          =>  "40001",
					'msg'           =>  '不存在此用户',
				);
				exit(json_encode($return_data));
		}
							
		session('user_auth', null);
        session('[destroy]');
		$return_data = array(
			'code'          =>  "40000",
			'msg'           =>  '退出成功',
		);
		exit(json_encode($return_data));
    }	
   
}
