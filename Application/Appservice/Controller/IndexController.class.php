<?php
namespace Home\Controller;
use Think\Controller;
class IndexController extends Controller {
			 protected function _initialize(){
					   // 获取当前用户ID
						define('UID',is_login());
						if( !UID ){// 还没登录 跳转到登录页面
								$this->redirect('/Home/Public/login');
						}
			 }
			 public function index()
			 {
			 			
			 			$this->display();
			 }
}