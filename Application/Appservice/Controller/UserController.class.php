<?php
namespace Appservice\Controller;
use Think\Controller;
class UserController extends Controller {
		 //得到用户数据
		 public function index()
		 {
			    $user_id=I("param.user_id",0);
				$User=M("ft_user")->where(" user_id=$user_id")->find();
				if(!$User){
						$return_data = array(
							'code'          =>  "40001",
							'msg'           =>  '不存在此用户',
						);
						
				}else{
						$return_data = array(
							'code'          =>  "40000",
							'msg'           =>  '读取成功',
							'data'           =>  $User,
						);
				}
				exit(json_encode($return_data));
	     }
		 
		 public function save_user(){
							$user_id=I("param.user_id",0,intval);
							$User=M("ft_user")->where(" user_id=$user_id")->find();
							if(!$User){
									$return_data = array(
										'code'          =>  "40001",
										'msg'           =>  '不存在此用户',
									);
									exit(json_encode($return_data));
							}
							$uname=I("param.uName");
							$openid=I("param.openid");
							
							$old_pass=I("param.pwd",0);
							$newPwd=I("param.newPwd",0);
							$confirmPwd=I("param.confirmPwd",0);
							if(empty($uname))
							{
							
									$return_data = array(
										'code'          =>  "40002",
										'msg'           =>  '姓名不能为空！',
									);
									exit(json_encode($return_data));
							}
							$data['uname'] =  $uname;
							$data['openid'] =  $openid;
							$data['last_time'] =  time();
							if($old_pass)
							{
										if(!$old_pass)
										{
													$return_data = array(
														'code'          =>  "40003",
														'msg'           =>  '旧密码不能为空',
													);
													exit(json_encode($return_data));
									
											
										}
										
										if(!isset($newPwd)) 
										{
												
													$return_data = array(
														'code'          =>  "40004",
														'msg'           =>  '新密码要输入'.$newPwd,
													);
												
													
													exit(json_encode($return_data));

											
										}
								
								
										if($newPwd!=$confirmPwd)
										{
													$return_data = array(
														'code'          =>  "40005",
														'msg'           =>  '新密码和确认密码不一致',
													);
													exit(json_encode($return_data));
													
											
										}
										
										
										if($old_pass!=md5(M('ft_user')->where('user_id='.$user_id)->getField('password')))
										{
											
												$return_data = array(
														'code'          =>  "40006",
														'msg'           =>  '旧密码不正确',
													);
													exit(json_encode($return_data));
										}
										$data['password'] =md5($newPwd);
							}
					
							$res = M('ft_user')->where('user_id='.$user_id)->save($data);
							if(0 < $res){
											$return_data = array(
														'code'          =>  "40000",
														'msg'           =>  '操作成功！',
											);
											exit(json_encode($return_data));
									
							} else {
											$return_data = array(
														'code'          =>  "40010",
														'msg'           =>  '操作失败',
											);
											exit(json_encode($return_data));
							}
		 }
}