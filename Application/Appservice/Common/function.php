<?php
function is_login(){
    $user_name = session('user_name');
    if (empty($user_name)) {
        return 0;
    } else {
        return 1;
    }
}


function alert($content='')
{
	return "<script>alert('$content');history.back();</script>";
	exit;
}


/**
 * 截取中文字符串
 */
function msubstr($str, $start=0, $length, $charset="utf-8", $suffix=false){
 if(function_exists("mb_substr")){
  if($suffix)
   return mb_substr($str, $start, $length, $charset)."...";
  else
   return mb_substr($str, $start, $length, $charset);
 }elseif(function_exists('iconv_substr')) {
  if($suffix)
   return iconv_substr($str,$start,$length,$charset)."...";
  else
   return iconv_substr($str,$start,$length,$charset);
 }
 $re['utf-8'] = "/[x01-x7f]|[xc2-xdf][x80-xbf]|[xe0-xef][x80-xbf]{2}|[xf0-xff][x80-xbf]{3}/";
 $re['gb2312'] = "/[x01-x7f]|[xb0-xf7][xa0-xfe]/";
 $re['gbk'] = "/[x01-x7f]|[x81-xfe][x40-xfe]/";
 $re['big5'] = "/[x01-x7f]|[x81-xfe]([x40-x7e]|xa1-xfe])/";
 preg_match_all($re[$charset], $str, $match);
 $slice = join("",array_slice($match[0], $start, $length));
 if($suffix) return $slice."…";
 return $slice;
}
//数组转字符串
function arrToStr($array)  
{  
    // 定义存储所有字符串的数组  
    static $r_arr = array();  
      
    if (is_array($array)) {  
        foreach ($array as $key => $value) {  
            if (is_array($value)) {  
                // 递归遍历  
                arrToStr($value);  
            } else {  
                $r_arr[] = $value;  
            }  
        }  
    } else if (is_string($array)) {  
            $r_arr[] = $array;  
    }  
          
    //数组去重  
    $r_arr = array_unique($r_arr);  
    $string = implode(",", $r_arr);  
      
    return $string;  
} 

