<?php
//+----------------------------------------------------------------------
//| 信息管理控制器
//+----------------------------------------------------------------------
// | date: 20150129
//+----------------------------------------------------------------------
//| Author: zsp
//+----------------------------------------------------------------------
class MessageController extends AdminController {	
    /**
     * 信息管理列表
     * @author zsp 2015-01-29
     */
    public function index()
    {
        $page = I("get.p",1,intval);
        $limit = C('PAGE_LIMIT_NUM');
        if($page < 1){
            $page = 1;
        }
        $list = M('hospital_staff_message')->page("$page,$limit")->select();
        $count = M('hospital_staff_message')->count();
        $Page       = new \Org\Util\Page($count,$limit);// 实例化分页类 传入总记录数
        $show       = $Page->show();// 分页显示输出
        $this->assign("page",$show);
        foreach ($list as $key => &$row) {
            if ($row['work_type'] == 1) {
                $row['work_type_text'] = "医生";
            } else {
                $row['work_type_text'] = "护士";
            }
            $sp = strrpos($row['work_photo_url'],"/");
            $before_img = substr($row['work_photo_url'],0,$sp+1);
            $after_img = substr($row['work_photo_url'],$sp+1);
            $row['thumb_work_img'] = $before_img .'thumb_' .$after_img;
        }
        $this->assign("_list",$list);
        $this->meta_title = '信息管理列表';
        $this->display();
    }

	 /**
     * 用户资料新增        
     * @author  zsp   2015-01-29
     */
    public function add($username = '', $content = '', $work_type = '',$upfile1='',$upfile2=''){
	   if(IS_POST){
           if($_FILES['upfile1']['name'] || $_FILES['upfile2']['name']){
               $data = $this->update_img();
           };
            $data['username'] =  $_POST['username'];
            $data['work_type'] =  $_POST['work_type'];
            $data['content'] =  $_POST['content'];


            $res    =   M('hospital_staff_message')->add($data);
            $datas['staff_id'] = $res;
            $ress   =   M('hospital_toupiao')->add($datas);
            if(0 < $res){
                $this->success('信息添加成功！',U('index'));
            } else {
                $this->error($this->showRegError($res));
            }
        } else {
            $this->meta_title = '新增信息';
            $this->display();
        }
    }

    /**
     * 修改信息
     * @author zsp 2015-01-29
     */
    public function edit(){

        if(IS_POST){
            if($_FILES['upfile1']['name'] || $_FILES['upfile2']['name']){
                $data = $this->update_img();
            };

            $data['username'] =  $_POST['username'];
            $data['work_type'] =  $_POST['work_type'];
            $data['content'] =  $_POST['content'];


            $res    =   M('hospital_staff_message')->where('id='.$_REQUEST['id'])->save($data);

            if(0 < $res){
                $this->success('信息修改成功！',U('index'));
            } else {
                $this->error($this->showRegError($res));
            }
        }else{
            $res = M('hospital_staff_message')->where('id='.$_REQUEST['id'])->select();
            $this->assign("res",$res);
            $this->meta_title = '修改信息';
            $this->display();
        }
    }

    /**
     * 删除信息
     * @author zsp 2015-01-29
     */
    public function del(){
       $res = M('hospital_staff_message')->where('id='.$_REQUEST['id'])->delete();
       $rees = M('hospital_toupiao')->where('staff_id='.$_REQUEST['id'])->delete();
        if(0 < $res){
            $this->success('信息删除成功！',U('index'));
        } else {
            $this->error($this->showRegError($res));
        }
    }

    /**
     * 获取错误信息
     * @param  integer $code 错误编码
     * @return string        错误信息
     */
    private function showRegError($code = 0){
        switch ($code) {
            case -1:  $error = '用户名长度必须在16个字符以内！'; break;
            case -2:  $error = '用户名被禁止注册！'; break;
            case -3:  $error = '用户名被占用！'; break;
            case -4:  $error = '密码长度必须在4-30个字符之间！'; break;
            case -5:  $error = '邮箱格式不正确！'; break;
            case -6:  $error = '邮箱长度必须在1-32个字符之间！'; break;
            case -7:  $error = '邮箱被禁止注册！'; break;
            case -8:  $error = '邮箱被占用！'; break;
            case -9:  $error = '手机格式不正确！'; break;
            case -10: $error = '手机被禁止注册！'; break;
            case -11: $error = '手机号被占用！'; break;
            default:  $error = '未知错误';
        }
        return $error;
    }


    public function update_img(){
        if($_FILES['upfile1']['name']){
            if($_FILES["upfile1"]["size"] > 2000000){
                echo "图片过大，修改信息失败";
                exit;
            }
            if ($_FILES["upfile1"]["error"] > 0){
                echo "Return Code: " . $_FILES["upfile1"]["error"] . "<br />";
                exit;
            }


            $img_type = $_FILES["upfile1"]["type"];
            if($img_type != "image/gif" && $img_type != "image/jpeg" && $img_type != "image/pjpeg" ) {
                echo "修改信息失败";
            }
            $date = date('Ymdhis');
            $fileName=$_FILES['upfile1']['name'];
            $name=explode('.',$fileName);
            $newPath=$date.'.'.$name[1];
            $oldPath=$_FILES['upfile1']['tmp_name'];
            $a=C(__UPLOAD__);
            $id_photo = $a."/upload/id_photo/" .  $newPath;
            move_uploaded_file( $oldPath,  $a."/upload/id_photo/" .  $newPath);
            $data['id_photo_url'] = '/'.$id_photo;
        }
        if($_FILES['upfile2']['name']){
            if($_FILES["upfile2"]["size"] > 2000000){
                echo "图片过大，修改信息失败";
                exit;
            }
            if ($_FILES["upfile2"]["error"] > 0){
              echo "Return Code: " . $_FILES["upfile2"]["error"] . "<br />";
              exit;
            }
            $img_type = $_FILES["upfile2"]["type"];
            if($img_type != "image/gif" && $img_type != "image/jpeg" && $img_type != "image/pjpeg" ) {
                echo "修改信息失败";
            }
            $date=date('Ymdhis');
            $fileName=$_FILES['upfile2']['name'];
            $name=explode('.',$fileName);
            $newPath=$date.'.'.$name[1];
            $oldPath=$_FILES['upfile2']['tmp_name'];

            $a=C(__UPLOAD__);
            $work_photo = $a."/upload/work_photo/" .$newPath;
            move_uploaded_file($oldPath,  $a."/upload/work_photo/" . $newPath);
            $image = new \Think\Image();
            $image->open($work_photo);
            // 按照原图的比例生成一个最大为150*150的缩略图并保存为thumb.jpg
            $thumb_path = $a."/upload/work_photo/thumb_" .  $newPath;
            $image->thumb(150, 150)->save($thumb_path);


            $data['work_photo_url'] =  '/'.$work_photo;
        }
     return $data;
    }
}	
?>	