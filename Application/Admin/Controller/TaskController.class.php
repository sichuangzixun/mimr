<?php

 Vendor('Baidu.sdk');
class TaskController extends AdminController {
	public function index(){
		
				
			$page = I("get.p",1,intval);
			$limit = C('PAGE_LIMIT_NUM');
			if($page < 1){
				$page = 1;
			}
			
			$user_id=I('param.user_id');
			$store_id=I('param.store_id');
			$start_time=I('param.start_time');
			$end_time=I('param.end_time');
			$keywords=I('param.keywords');
			if($user_id) {          
				  $map['user_id']=$user_id;      
			}
			
			if($store_id) {          
				  $map['store_id']=$store_id;      
			}
		
			if($start_time){
				 $start_time = strtotime($start_time." 00:00:00");
				 $map['task_time'] = array("EGT",  $start_time);
			}
			if($end_time){
				$end_time = strtotime($end_time." 23:59:59");
				$map['task_time'] = array("ELT", $start_time);
			}	

			$Model = M('ft_schedule');
			if(!empty($keywords)) {     
					//$sql=$M('ft_store')->where("store_id=".$intval($keywords))->field('store_id')->buildSql();      
			}
			
			
			$list  = $Model->where($map)->page("$page,$limit")->order(" builddate desc")->select();        
			$count = $Model->where($map)->count();
			$Page       = new \Org\Util\Page($count,$limit);// 实例化分页类 传入总记录数
			$show       = $Page->show();// 分页显示输出
			$this->assign("page",$show);
				
			   
			
				
			if($list) {
					foreach($list as &$key){
						$key['uname'] = D("ft_user")->where(" user_id='".$key['user_id']."'")->getField("uname");
						$key['store_name'] = D("ft_store")->where(" store_id='".$key['store_id']."'")->getField("store_name");
						
					}
					$this->assign('_list', $list);
			}
			
		 $user= D("ft_user")->order(" user_id DESC ")->select();    
	     $this->assign('user', $user);   
			
		 $store= D("ft_store")->order(" store_id DESC ")->select();    
	     $this->assign('store', $store);    
		 
			 $this->display();
	}
	
	//日志
	public function task_log(){
			$id = I("get.id",0,intval);
		    $page = I("get.p",1,intval);
			$limit = C('PAGE_LIMIT_NUM');
			if($page < 1){
				$page = 1;
			}
				
			$list  = D("ft_task_log")->where(" task_id=$id")->page("$page,$limit")->order(" create_time desc")->select();        
			$count = M('ft_task_log')->where($map)->count();
			$Page       = new \Org\Util\Page($count,$limit);// 实例化分页类 传入总记录数
			$show       = $Page->show();// 分页显示输出
			$this->assign("page",$show);
				
			$this->assign('_list', $list);
			$this->display();
	}
	
	public function index_date(){
		  $user= D("ft_user")->where($map)->order(" user_id DESC ")->select();    
	      $this->assign('user', $user);   
			
		 $store= D("ft_store")->where($map)->order(" store_id DESC ")->select();    
	     $this->assign('store', $store);    
			 
		 $project= D("ft_project")->where($map)->order(" id DESC ")->select();    
	     $this->assign('project', $project);    
		 $this->display();
	}
	
	
	public function list_data(){
			$rows= D("ft_schedule")->where(" store_id is not null")->order(" id DESC ")->select();    
		
			 foreach($rows as $r =>$value){
				 $a = explode(' ',  $value['builddate']);
				 $rows[$r]['builddate'] = $a[0];
				 $rows[$r]['uname'] = D("ft_user")->where(' user_id='.$value['user_id'])->getField('uName');
				 $rows[$r]['store_name'] = D("ft_store")->where(' store_id='.$value['store_id'])->getField('store_name');
			 }
			 echo json_encode($rows);
 
 
		 
	}
	
	 public function add(){
	 		$taskInfo=$_POST["taskInfo"];
			$buildDate=$_POST["buildDate"];
			$user_id=$_POST["user_id"];
			$store_id=$_POST["store_id"];
			if(empty($taskInfo))
			{
				$this->error("标题不能为空！");
				exit;
			}
			
			$data['task'] =  $taskInfo;
			$data['builddate'] =  $buildDate;
			$data['user_id'] =  $user_id;			
			$data['store_id'] =  $store_id;
			$data['project_id'] =  I("param.project_id",0,intval);
			$data['task_time'] =  strtotime($buildDate);
			$task_id=M('ft_schedule')->add($data);
			
			//以下是推送
			$Store=M("ft_store")->where(" store_id=$store_id")->find();
			$store_name=$Store["store_name"];
			$store_address=$Store["store_address"];
			
			$channelid=M('ft_user')->where(" user_id=".$user_id)->getField('channelid');
			if($channelid){
						$title="你有新任务";
						$content="MDC系统提示:分店简称:".$store_name."\n分店地址:".$store_address."\n执行时间:".$buildDate."\n备注:".$taskInfo."\n";
						
						$url="http://".$_SERVER['SERVER_NAME'].U("/Appservice/Task/view?task_id=$task_id");
						$this->Baidu_push($channelid,$title,$content,$url);
			}
           
	 }
	
	  //推送
	  public function Baidu_push($channelid,$title,$content,$url){
				$sdk = new \PushSDK();
				
				
				$message = array (
					'title' => $title,
					'description' => $content,
					'custom_content'=>array(
						'url' => $url,
					),
				 );
				 // 设置消息类型为 通知类型.
				$opts = array (
					'msg_type' => 1
				);
				$sdk -> pushMsgToSingleDevice($channelid, $message, $opts);  // pushBatchUniMsg
				//$sdk -> pushMsgToSingleDevice("3517542699694488796", $message, $opts);
	  }
	 /*
 * 编辑功能
 */
    public function edit($id = 0){
			if(IS_POST){
    		$id = I('post.id',0);
    		$user_id=$_POST["user_id"];
			$store_id=$_POST["store_id"];
			$builddate=$_POST["builddate"];
			$task=$_POST["task"];
			
			if(empty($user_id) or empty($store_id) or empty($builddate) or empty($task))
			{
				$this->error("数据不完整");
				exit;
			}
			$data['project_id'] = I("param.project_id",0,intval);
			$data['user_id'] =  $user_id;
			$data['store_id'] =  $store_id;
			$data['builddate'] =  $builddate;
			$data['task_time'] =  strtotime($builddate);
			$data['task'] =  $task;
			
			$user_id_old=M('ft_schedule')->where('id='.$id)->getField("user_id");
			$store_id_old=M('ft_schedule')->where('id='.$id)->getField("store_id");
			$builddate_old=M('ft_schedule')->where('id='.$id)->getField("builddate");
			$task_old=M('ft_schedule')->where('id='.$id)->getField("task");
			if($user_id!=$user_id_old)
			{

$data2["log_type"]=iconv("GB2312","UTF-8","人员") ;
$data2["task_log_content"]=M('ft_user')->where('user_id='.$user_id_old)->getField("uName")."->".M('ft_user')->where('user_id='.$user_id)->getField("uName");

						//=====================================推送开始=========================
						$Store=M("ft_store")->where(" store_id=$store_id")->find();
						$store_name=$Store["store_name"];
						$store_address=$Store["store_address"];
						
						$channelid=M('ft_user')->where(" user_id=".$user_id)->getField('channelid');
						if($channelid){
									$title="你有新任务";
									$content="MDC系统提示:分店简称:".$store_name."\n分店地址:".$store_address."\n执行时间:".$buildDate;
									$url="http://".$_SERVER['SERVER_NAME'].U("/Appservice/Task/view?task_id=$id");
									$this->Baidu_push($channelid,$title,$content,$url);
						}
						//=====================================推送结束============================
			}
			
			if($store_id!=$store_id_old)
			{

$data2["log_type"]=iconv("GB2312","UTF-8","门店") ;
$data2["task_log_content"]=M('ft_store')->where('store_id='.$store_id_old)->getField("store_name")."->".M('ft_store')->where('store_id='.$store_id)->getField("store_name");
			}
			
			
			if($builddate!=$builddate_old)
			{
				
				$data2["log_type"]=iconv("GB2312","UTF-8","时间") ;
				$data2["task_log_content"]=$builddate_old."->".$builddate;
			}
			
			if($task!=$task_old)
			{
				$data2["log_type"]=iconv("GB2312","UTF-8","备注") ;
				$data2["task_log_content"]=$task_old."->".$task;
			}
			
			
			$res = M('ft_schedule')->where('id='.$id)->save($data);
			
			//=====================================推送开始=========================
			$Store=M("ft_store")->where(" store_id=$store_id")->find();
			$store_name=$Store["store_name"];
			$store_address=$Store["store_address"];
			
			$channelid=M('ft_user')->where(" user_id=".$user_id_old)->getField('channelid');
			if($channelid){
						$title="任务信息变动";
						$content="MDC系统提示:分店简称:".$store_name."\n分店地址:".$store_address."\n执行时间:".$buildDate;
						$url="http://".$_SERVER['SERVER_NAME'].U("/Appservice/Task/view?task_id=$id");
						$this->Baidu_push($channelid,$title,$content,$url);
			}
			//=====================================推送结束============================
			if($res){
				$data2['create_time']=time();
				$data2['task_id']=$id;
				M('ft_task_log')->add($data2);
				
				
    			$this->success('操作成功！',U('./Admin/Task'));
    		} else {
    			$this->error($res);  //$this->showRegError(
    		}
				
    	} else {
				$info = D('ft_schedule')->field(true)->find($id);
				 if(false === $info){
					$this->error('获取后台菜单信息错误');
				}
			   $user = D('ft_user')->where(" ublock=0")->order(" user_id desc")->select();
			   $store = D('ft_store')->order(" store_id desc")->select();
			   $this->assign('info', $info); 
			   $this->assign('user', $user); 
			   $this->assign('store', $store); 
			   
			   //项目列表
			   $project= D("ft_project")->where($map)->order(" id DESC ")->select();    
			   $this->assign('project', $project);   
			   $this->display();
    	}
    	
    }
		/**
		 * 删除分类
	    */
		public function del_date(){
				$id = array_unique((array)I('post.taskId',0));
				if ( empty($id) ) {
					$this->error('请选择要操作的数据!');
				}
				$map = array('id' => array('in', $id) );
				D('ft_schedule')->where($map)->delete();
		}
		
		public function del(){
				$id = array_unique((array)I('post.id',0));
				if ( empty($id) ) {
					$this->error('请选择要操作的数据!');
				}
				$map = array('id' => array('in', $id) );
				D('ft_schedule')->where($map)->delete();
				
				$this->success('删除成功');
		}
		
		public function getinfo(){
				  $typex=$_REQUEST['typex'];
				  $intID=$_REQUEST['intID'];
				  if($typex=="store_name")
				  {
						$result=D("ft_store")->where(" store_id=".$intID)->getField('store_name');    
				  }
				  elseif($typex=="uName")
				  {
				  		$result=D("ft_user")->where(" user_id=".$intID)->getField('uName');    
				  }
				  else
				  {
				  		$result=D("ft_user")->where(" user_id=".$intID)->getField('openid');    
				  }
				  echo $result;
		 
	}
	
	
	 public function upload(){
				   if(IS_POST){
						    header("Content-Type:text/html;charset=utf-8");
							$upload = new \Think\Upload();// 实例化上传类
							$upload->maxSize   =     3145728 ;// 设置附件上传大小
							$upload->exts      =     array('xls', 'xlsx');// 设置附件上传类
							
							$upload->rootPath  =     C(__UPLOAD__); // 设置附件上传根目录
							$upload->savePath  =      'xls/'; // 设置附件上传目录
							$upload->autoSub  = false;
							// 上传文件
							$info   =   $upload->uploadOne($_FILES['file1']);
							$filename = C(__UPLOAD__).'xls/'.$info['savename'];
							$exts = $info['ext'];
						    
							//print_r($info);exit;
							//$filename= C(__UPLOAD__).'xls/'."drugs_moban.xlsx";
							if(!$info) {// 上传错误提示错误信息
								  $this->error($upload->getError());
							  }else{// 上传成功
									  $this->goods_import($filename, $exts);
									 // $this->success('操作成功');
									 
							}

								
				   }
				   else
				   {
					 	 $this->display(); 
				   }
					   
		  }
		  
		  		   //导入数据方法
    protected function goods_import($filename, $exts='xls')
    {
        //导入PHPExcel类库，因为PHPExcel没有用命名空间，只能inport导入
        import("Org.Util.PHPExcel");
        //创建PHPExcel对象，注意，不能少了\
        $PHPExcel=new \PHPExcel();
        //如果excel文件后缀名为.xls，导入这个类
        if($exts == 'xls'){
            import("Org.Util.PHPExcel.Reader.Excel5");
            $PHPReader=new \PHPExcel_Reader_Excel5();
        }else if($exts == 'xlsx'){
            import("Org.Util.PHPExcel.Reader.Excel2007");
            $PHPReader=new \PHPExcel_Reader_Excel2007();
        }


        //载入文件
        $PHPExcel=$PHPReader->load($filename);
        //获取表中的第一个工作表，如果要获取第二个，把0改为1，依次类推
        $currentSheet=$PHPExcel->getSheet(0);
        //获取总列数
        $allColumn=$currentSheet->getHighestColumn();
        //获取总行数
        $allRow=$currentSheet->getHighestRow();
        //循环获取表中的数据，$currentRow表示当前行，从哪行开始读取数据，索引值从0开始
        for($currentRow=1;$currentRow<=$allRow;$currentRow++){
            //从哪列开始，A表示第一列
            for($currentColumn='A';$currentColumn<=$allColumn;$currentColumn++){
                //数据坐标
                $address=$currentColumn.$currentRow;
                //读取到的数据，保存到数组$arr中
                $data[$currentRow][$currentColumn]=$currentSheet->getCell($address)->getValue();
            }

        }
        	$this->save_import($data);
			unlink($filename);
			//var_dump($data);
    }


	function https_request($url, $data = null)
	{
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
		if (!empty($data)){
			curl_setopt($curl, CURLOPT_POST, 1);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		}
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($curl);
		curl_close($curl);
		return $output;
	}

	 //保存导入数据
    public function save_import($data)
    {
        //print_r($data);exit;

        $Goods = M('ft_schedule');
        $add_time = time();
        foreach ($data as $k=>$v){
            if($k >= 2){
                $uName=$v['D'];
				$user_id= intval(M('ft_user')->where(" uName='$uName'")->getField('user_id'));
                $info[$k-2]['user_id'] =$user_id;

              
				$store_name=$v['C'];
				$store_id=M('ft_store')->where(" store_name='$store_name'")->getField('store_id');
                $info[$k-2]['store_id'] = $store_id;
				
				$builddate=$v['E'];
                $info[$k-2]['builddate'] = $builddate;  //任务时间
				$info[$k-2]['task_time'] = strtotime($builddate);
				
				$task=$v['I'];
                $info[$k-2]['task'] = $task;
            
				$project_id=$v['J'];
                $info[$k-2]['project_id'] = $project_id;

				$buildDate=$builddate;
				$taskInfo="批量导入任务";
				$openid=M('ft_user')->where(" user_id=".$user_id)->getField('openid'); //echo 'user_id='.$user_id;exit;
				$url="?type=msg";
				$url.="&buildDate=$buildDate";
				$url.="&store_name=$store_name";
				$url.="&taskInfo=$taskInfo";
				$url.="&openid=$openid";
				
				
				
				if($store_id)
				{
						$count=M('ft_schedule')->where(" store_id=$store_id and user_id=$user_id and task_time=".strtotime($builddate))->count();
						if(intval($count)>0)
						{
								continue;
						}
						$this->https_request("http://m.mimr.ipanyu.cn/weixin/".$url);
						$task_id = $Goods->add($info[$k-2]);
						
						
						//以下是推送
						$Store=M("ft_store")->where(" store_id=$store_id")->find();
						$store_name=$Store["store_name"];
						$store_address=$Store["store_address"];
						
						$channelid=M('ft_user')->where(" user_id=".$user_id)->getField('channelid');
						if($channelid){
									$title="你有新任务";
									$content="MDC系统提示:分店简称:".$store_name."\n分店地址:".$store_address."\n执行时间:".$buildDate."\n备注:".$taskInfo."\n";
									$url="http://".$_SERVER['SERVER_NAME'].U("/Appservice/Task/view?task_id=$task_id");
									$this->Baidu_push($channelid,$title,$content,$url);
						}
						//以下是推送
				}
                 

            }

        }

        if($task_id){
			echo "<script>window.parent.location.reload();</script>";
            //$this->success('产品导入成功');
        }else{
            $this->error('no data error');
        }
        //print_r($info);

    }
	
	
}
?>