<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: datahome改写 <datahome@qq.com>  2014-3-17
// +----------------------------------------------------------------------


/**
 * 后台用户控制器
 * @author  覃(qinshuxin@21cn.com)  2015-3-31
 */
class ClickController extends AdminController {

    public function index(){
			
		    	$page = I("get.p",1,intval);
		    	$limit = C('PAGE_LIMIT_NUM');
		    	if($page < 1){
		    		$page = 1;
		    	}
				$list  = D("website_click_log")->order(" click_time desc ")->select();     
				$count = M('website_click_log')->count();
				$Page       = new \Org\Util\Page($count,$limit);// 实例化分页类 传入总记录数
				$show       = $Page->show();// 分页显示输出
				$this->assign("page",$show);
		 		/*
		 		if($list) {
					foreach($list as &$key){
						if($key['parent_id']){
							$key['title'] = D("website_category")->where(" cat_id=".$key['parent_id'])->getField(" cat_name");
						}
						else
						{
							$key["title"]="顶级分类";
						}
						
					}
					
				}
				*/
				$this->assign('_list', $list);
				$this->display();
    }


  
}
