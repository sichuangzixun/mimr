<?php
class CategoryController extends AdminController {

    public function index(){
				$project_id = I('param.project_id',0,intval);
				//$map = array('cat_type' => array('EQ', $cat_type) );
			    if(IS_POST){
					 if(is_array($_POST[sort_order])) {
						foreach($_POST[sort_order] as $cat_id => $value) {
							  $data['sort_order'] =   $value;
							  M('ft_category')->where('cat_id='.$cat_id)->save($data);
						}
					 }
				}
				
				$info=M("ft_project")->where(" id=$project_id")->find();
				//$list=D('ft_category')->select();
				$this->assign('info',$info);
			    $this->display();
				
    }


   //===========================================================分类====================================================
   public function add(){
	   	  if(IS_POST){
				$cat_name=$_POST['cat_name'];
				$cat_type = I('post.cat_type',0);
				$project_id = I('post.project_id',0,intval);
				if(empty($cat_name))
				{
					$this->error("分类名称不能为空！");
				}
				
				if(!$project_id)
				{
					$this->error("没有项目ID");
				}
			    $parent_id=isset($_POST["parent_id"])?intval($_POST["parent_id"]):0; 
				$sort_order =  intval($_POST['sort_order'])?$_POST['sort_order']:0;     
				$is_show=isset($_POST["is_show"])?intval($_POST["is_show"]):0;
				$is_jianchadian=isset($_POST["is_jianchadian"])?intval($_POST["is_jianchadian"]):0;
				$is_order=isset($_POST["is_order"])?intval($_POST["is_order"]):0;  
				
				$is_picture=isset($_POST["is_picture"])?intval($_POST["is_picture"]):0;    //是否图片还是名称
				$is_more=isset($_POST["is_more"])?intval($_POST["is_more"]):0;             //单张图片还是多张图片
				

				
				$data['cat_name'] =   $cat_name;
				$data['parent_id'] =  $parent_id;
				$data['sort_order'] = $sort_order;
				$data['is_show'] =  $is_show;
				$data['is_jianchadian'] =  $is_jianchadian;
				$data['is_order'] =  $is_order;
				$data['create_time'] = time();
				$data['project_id'] =   $project_id;
				/*$data['is_picture'] =  $is_picture;
				$data['is_more'] =  $is_more;*/
				//$data['cat_type'] =  $cat_type;
				
			    $res = M('ft_category')->add($data);  //增加数据
				
				if(intval($is_jianchadian)==1)
				{
$get_cat_id=M('ft_category')->where(" cat_name='$cat_name' and parent_id=".$parent_id)->order(" cat_id desc")->getField("cat_id");
				
						//==========================================表单内容==============
						$data2['create_time'] = time();
						$data2['parent_id'] = $get_cat_id;
						$data2['field_name'] = "file".date("YmdHis",time());
						$data2['field_title'] = "检查点图片";
						$data2['field_type'] = "file";
						$data2['request'] = 1;
						$data2['project_id'] =   $project_id;
						$res1 = M('ft_form_field')->add($data2);
				}
				//==========================================表单内容==============
				
				//$this->error($is_jianchadian);
				//exit;
				if(0 < $res){
					$this->success('操作成功！',U('Admin/Category/index?project_id='.$project_id.'#miaodian_'.$res));
				} else {
					$this->error($this->showRegError($res));
				}
        } else {
			    
			   $this->display();
        }
		
		
           
			
			
		   
    }

/*
 * 编辑功能
 */
    public function edit($id = 0){
    	
    	if(IS_POST){
    		$id = I('post.id',0);
    		$cat_name=$_POST['cat_name'];
				$cat_type = I('post.cat_type',0);
				if(empty($cat_name))
				{
					$this->error("分类名称不能为空！");
					exit;
				}
			    $parent_id=isset($_POST["parent_id"])?intval($_POST["parent_id"]):0; 
				$sort_order =  intval($_POST['sort_order'])?$_POST['sort_order']:0;     
				$is_show=isset($_POST["is_show"])?intval($_POST["is_show"]):0;
				$is_jianchadian=isset($_POST["is_jianchadian"])?intval($_POST["is_jianchadian"]):0;
				$is_order=isset($_POST["is_order"])?intval($_POST["is_order"]):0;  
				
				$is_picture=isset($_POST["is_picture"])?intval($_POST["is_picture"]):0;    //是否图片还是名称
				$is_more=isset($_POST["is_more"])?intval($_POST["is_more"]):0;             //单张图片还是多张图片
				
				$ft_cat=M('ft_category')->where(" cat_id=".$id)->find();
				$old_is_jianchadian=$ft_cat["is_jianchadian"];
				$project_id=$ft_cat["project_id"];
				if(intval($is_jianchadian)!=$old_is_jianchadian)
				{
				        //==========================================表单内容==============
						if(intval($is_jianchadian)==1)
						{
							$data2['create_time'] = time();
							$data2['parent_id'] = $id;
							$data2['field_name'] = "file".date("YmdHis",time());
							$data2['field_title'] = "检查点图片";
							$data2['field_type'] = "file";
							$data2['request'] = 1;
							$data2['project_id'] =   $project_id;
							$res1 = M('ft_form_field')->add($data2);
						}else
						{
							D('ft_form_field')->where(" parent_id=".$id)->delete();
						}
					
				}
				$data['cat_name'] =   $cat_name;
				$data['parent_id'] =  $parent_id;
				$data['sort_order'] = $sort_order;
				$data['is_show'] =  $is_show;
				$data['is_jianchadian'] =  $is_jianchadian;
				$data['is_order'] =  $is_order;
				
				/*$data['is_picture'] =  $is_picture;
				$data['is_more'] =  $is_more;*/
			
				
    		$res = M('ft_category')->where('cat_id='.$id)->save($data);
    		if(0 < $res){
    			$this->success('操作成功！',U('Admin/Project/index/#miaodian_'.$id));
    		} else {
    			$this->error($this->showRegError($res));
    		}
    	} else {
    		 $info = D('ft_category')->field(true)->find($id);
          	 if(false === $info){
                $this->error('获取后台菜单信息错误');
            }
        	$this->assign('info', $info);
            $this->display();
    	}
    }
    

		/**
		 * 删除分类
	    */
		public function del(){
			$id = array_unique((array)I('get.id',0));
			//$cat_type = I('get.cat_type',0);
			if ( empty($id) ) {
				$this->error('请选择要操作的数据!');
			}
			$count=D('ft_category')->where(" parent_id=".I('get.id',0))->count();
			if($count)
			{
				$this->error('你还有下级数据，请先删除!');
			}
	
			$map = array('cat_id' => array('in', $id) );
			D('ft_form_field')->where(" parent_id=".I('get.id',0))->delete();
			if(D('ft_category')->where($map)->delete()){
				$this->success('删除成功');
			} else {
				$this->error('删除失败！');
			}
		}

}
