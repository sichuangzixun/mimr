<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: datahome改写 <datahome@qq.com>  2014-3-17
// +----------------------------------------------------------------------


/**
 * 后台用户行为控制器
 * @author  datahome改写 <datahome@qq.com>  2014-2-26
 */
class UserActionController extends AdminController {

    /**
     * 用户行为列表
     * @author huajie <banhuajie@163.com>
     */
    public function index(){
        $page = I("get.p",1,intval);
        $limit = C('PAGE_LIMIT_NUM');
        
        if($page < 1){
            $page = 1;
        }
        //获取列表数据
        $model = 'Action' ;  //模型名称 
        
        //查询过滤条件
        $map['status']    =   array('gt', -1);        
 
        //使用后台排序
        //$list   =   $this->lists($model, $map);
        
        //使用前台排序
		$list  =  D($model)->where($map)->page("$page,$limit")->order(" id DESC ")->select();        
 
        $count = D($model)->where($map)->count();
        
        $Page       = new \Org\Util\Page($count,$limit);// 实例化分页类 传入总记录数
        $show       = $Page->show();// 分页显示输出
       
        $this->assign("page",$show);
        
        int_to_string($list);
        // 记录当前列表页的cookie
        Cookie('__forward__',$_SERVER['REQUEST_URI']);

        $this->assign('_list', $list);
        $this->meta_title = '用户行为';
        $this->display("");
    }

    /**
     * 新增行为
     * @author huajie <banhuajie@163.com>
     */
    public function addAction(){
        $this->meta_title = '新增行为';
        $this->assign('data',null);
        $this->display('editaction');
    }

    /**
     * 编辑行为
     * @author huajie <banhuajie@163.com>
     */
    public function editAction(){
        $id = I('get.id');
        empty($id) && $this->error('参数不能为空！');
        $data = D('Action')->field(true)->find($id);

        $this->assign('data',$data);
        $this->meta_title = '编辑行为';
        $this->display();
    }

    /**
     * 更新行为
     * @author huajie <banhuajie@163.com>
     */
    public function saveAction(){
        $res = D('Action')->update();
        if(!$res){
            $this->error(D('Action')->getError());
        }else{
            $this->success($res['id']?'更新成功！':'新增成功！', Cookie('__forward__'));
        }
    }


}
