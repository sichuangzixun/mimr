<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: datahome改写 <datahome@qq.com>  2014-3-17
// +----------------------------------------------------------------------


/**
 * 后台用户控制器
 * @author  覃(qinshuxin@21cn.com)  2015-3-31
 */
class ResultController extends AdminController {
	 public function project(){
				$type_id=I("param.type_id",0,intval);  
				$map=" id>0";
				if($type_id){
						$map.=" and type_id=$$type_id";
				}
				$page = I("get.p",1,intval);
		    	$limit = C('PAGE_LIMIT_NUM');
		    	if($page < 1){
		    		$page = 1;
		    	}
				$list  = D("ft_project")->where($map)->page("$page,$limit")->order(" id desc")->select();  
				foreach($list as &$row){
					$row['result_count']=M("ft_result")->where(" project_id=".$row['id'])->count();
				}
				$this->assign('_list', $list);
			
				$count = M('ft_project')->where($map)->count();
				$Page       = new \Org\Util\Page($count,$limit);// 实例化分页类 传入总记录数
				$sum = ceil($count/$limit);
				
				$show       = $Page->show();// 分页显示输出
				$this->assign("page",$show);  
				$this->display();
    }
	
    public function index(){
				$project_id = I('param.project_id',0,intval);
				$keywords = I('get.keywords','');
				$map=" project_id=$project_id";
	
				
				$page = I("get.p",1,intval);
		    	$limit = C('PAGE_LIMIT_NUM');
		    	if($page < 1){
		    		$page = 1;
		    	}
				$list  = D("ft_result")->where($map)->page("$page,$limit")->order(" result_id desc")->select();        
				$count = M('ft_result')->where($map)->count();
				$Page       = new \Org\Util\Page($count,$limit);// 实例化分页类 传入总记录数
				$show       = $Page->show();// 分页显示输出
				$this->assign("page",$show);
				
				
		 	
				
				if($list) {
					foreach($list as &$key){
						if($key['result_id']){
							$key['store_name'] = D("ft_store")->where(" store_id='".$key['store_id']."'")->getField(" store_name");
							$key['uName'] = D("ft_user")->where(" user_id='".$key['user_id']."'")->getField(" uName");
							
							
							//$data['project_id']=M("ft_schedule")->where(" id=".$key['task_id'])->getField("project_id");
							//M("ft_result")->where(" task_id=".$key['task_id'])->save($data);
						}
					}
					$this->assign('_list', $list);
				}
				$this->display();
    }
	
	

	//浏览上传结果
	public function view(){
				$task_id = $_GET['task_id'];
				if(empty($task_id))
				{
					$this->error("没有正确的任务IDtask_id");
					exit;
				}
				
				$map=" task_id=".$task_id;
				$count = M('ft_form_field_value')->where($map)->count();
				
				if($count<=0)
				{
					$this->error("不存在此条记录");
					exit;
				}
				$page = I("get.p",1,intval);
		    	$limit = C('PAGE_LIMIT_NUM');
		    	if($page < 1){
		    		$page = 1;
		    	}
				$list  = D("ft_form_field_value")->where($map)->page("$page,$limit")->order(" create_time desc")->group('parent_id')->select();        
				$count = M('ft_form_field_value')->where($map)->count("distinct(parent_id)");
				$Page       = new \Org\Util\Page($count,$limit);// 实例化分页类 传入总记录数
				$show       = $Page->show();// 分页显示输出
				$this->assign("page",$show);
				
				$store_id = D("ft_schedule")->where(" id=".$task_id)->getField(" store_id");
				$task_name= D("ft_store")->where(" store_id=".$store_id)->getField(" store_name");
				$this->assign("task_name",$task_name);
				//end(explode('.', $file))
				if($list) {
					foreach($list as &$key){
						if($key['task_id']){
							//$task_time = D("ft_schedule")->where(" id=".$key['task_id'])->getField(" task_time");
							$key['cat_name'] = D("ft_category")->where(" cat_id=".$key['parent_id'])->getField(" cat_name");
							$key['uname'] = D("ft_user")->where(" user_id=".$key['user_id'])->getField(" uName ");  //上传者
							$hou_zui=end(explode('.',$key['field_value']));
							if($hou_zui=="jpg" or $hou_zui=="jpeg" or $hou_zui=="png" or$hou_zui=="gif")
							{
								$key['hou_zui']=$key['field_value'];
							}
							$key['count'] = D("ft_form_field_value")->where(" parent_id=".$key['parent_id']." and task_id=".$task_id)->count();  //这个检查点有多少条记录
							$key['list2'] = D("ft_form_field_value")->where(" parent_id=".$key['parent_id']." and task_id=".$task_id)->select(); 
						}
					}
					$this->assign('_list', $list);
				}
				$this->display();
    }
	
				//递归全部分类
			private	function getByCategory($parent_id=0,$class=0) 
			{
									$tmp = '&nbsp;&nbsp;&nbsp;&nbsp;';
									for($i=0;$i<$class;$i++){
										$tmp .= '&nbsp;&nbsp;&nbsp;&nbsp;';
										$kongge .= '&nbsp;&nbsp;&nbsp;&nbsp;';
									}
									$tmp .= '|--';

									switch($class){
										case 0:
											$bgvalue = ' ';
											break;
										default:
											$bgvalue = ' bgColor="#FFFFFF"';
											break;
									}
									
									$list = D("ft_category")->where('parent_id='.$parent_id)->order("sort_order")->select();
									foreach ($list as $k => $v) {
										 if($class)
										 {
											  //$PHPExcel->getActiveSheet()->setCellValue('A'.$i,$v['cat_name']);  
											 //echo $kongge.$v['cat_name'];
											  if($v['is_jianchadian'])
											  {
													echo $v['cat_name']."<a href=".U('Admin/Form/index','id='.$v['cat_id']).">设置题目</a><br>";
											  }
										 }	
											$j = $j + 1;
											$this->getByCategory($v['cat_id'],$class+1);
									 }
			}
	//批量导出结果
	/*
*  @creates a compressed zip file  将多个文件压缩成一个zip文件的函数  
*   @$files 数组类型  实例array("1.jpg","2.jpg");    
*   @destination  目标文件的路径  如"c:/androidyue.zip"  
*   @$overwrite 是否为覆盖与目标文件相同的文件  
*   @Recorded By Androidyue  
*   @Blog:http://thinkblog.sinaapp.com  
 */    
function create_zip($files = array(),$destination = '',$overwrite = false) {    
    //if the zip file already exists and overwrite is false, return false    
    //如果zip文件已经存在并且设置为不重写返回false    
    if(file_exists($destination) && !$overwrite) { return false; }    
    //vars    
    $valid_files = array();    
    //if files were passed in...    
    //获取到真实有效的文件名    
    if(is_array($files)) {    
        //cycle through each file    
        foreach($files as $file) {    
        //make sure the file exists    
            if(file_exists($file)) {    
            $valid_files[] = $file;    
            }    
        }    
    }    
    //if we have good files...    
    //如果存在真实有效的文件    
    if(count($valid_files)) {    
        //create the archive    
        $zip = new ZipArchive();    
        //打开文件       如果文件已经存在则覆盖，如果没有则创建    
        if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {    
            return false;    
        }    
        //add the files    
        //向压缩文件中添加文件    
        foreach($valid_files as $file) {   
            $file_info_arr= pathinfo($file);  
        $filename =$file_info_arr['basename'];   
                $zip->addFile($file,$filename);    
        }    
        //debug    
        //echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;    
        //close the zip -- done!    
        //关闭文件    
        $zip->close();    
        //check to make sure the file exists    
        //检测文件是否存在    
        return file_exists($destination);    
    }else{    
        //如果没有真实有效的文件返回false    
        return false;    
    }    
}   

/*
	public function make_xls(){
					$task_id = I('post.task_id',0);
					if ( empty($task_id) ) {
						$this->error('请选择要操作的数据!');
					}
					$task_id=substr($task_id,0,strlen($task_id)-1);
					$map = " id in (".$task_id.")";

					
					
				    $filename="Public/xls/data_moban.xlsx";
					//导入PHPExcel类库，因为PHPExcel没有用命名空间，只能inport导入
					import("Org.Util.PHPExcel");
					//创建PHPExcel对象，注意，不能少了\
					$PHPExcel=new \PHPExcel();
					//如果excel文件后缀名为.xls，导入这个类
					$exts='xlsx';
					if($exts == 'xls'){
						import("Org.Util.PHPExcel.Reader.Excel5");
						$PHPReader=new \PHPExcel_Reader_Excel5();
					}else if($exts == 'xlsx'){
						import("Org.Util.PHPExcel.Reader.Excel2007");
						$PHPReader=new \PHPExcel_Reader_Excel2007();
					}
					  //载入文件
					$PHPExcel=$PHPReader->load($filename);
					
				   //$task_id = I('get.task_id',0);
				   $all  = D("ft_schedule")->where($map)->select();
				   foreach ($all as &$key1) {
								   $list  = D("ft_category")->where('is_jianchadian=1')->order(" sort_order asc ")->select(); 
								   $i = 1;
								   foreach ($list as &$key) {
												$i++;   
											   $PHPExcel->getActiveSheet()->setCellValue('A'.$i,$key['cat_name']);  
											   $cat_id=$key['cat_id'];
											   $list2 = D("ft_form_field_value")->where(" task_id=".$key1['id']." and parent_id=".$cat_id)->field('field_id,field_value')->select();
											   $count2  = D("ft_form_field_value")->where(" task_id=".$key1['id']." and parent_id=".$cat_id)->count(); 
											   $i2=0;
											   foreach ($list2 as &$key2) {
													$i2++;
													$field_value.=$i2.".". D("ft_form_field")->where(" field_id=".$key2['field_id'])->getField('field_title')."(".$key2['field_value'].")\n";
													$PHPExcel->getActiveSheet()->setCellValue('B'.$i,$count2."条记录\n".$field_value);  
											   }
											   $field_value='';
											  
											  
											 
								   }
								   $user_id=$key1['user_id'];
								   $store_id=$key1['store_id'];
								   $builddate=$key1['builddate'];
								   $uName=M("ft_user")->where(" user_id=".$user_id)->getField('uName');
								   $store_name=M("ft_store")->where(" store_id=".$store_id)->getField('store_name');
								   
								 
								   
								   $PHPExcel->getActiveSheet()->setCellValue('C2',"任务人:".$uName."\n任务时间:".$builddate."\n门店:". $store_name);  
									// header('pragma:public');
									//header('Content-type:application/vnd.ms-excel;charset=utf-8;name="111.xls"');
									//header("Content-Disposition:attachment;filename=$fileName.xls");//attachment新窗口打印inline本窗口打印
									$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007'); 

									$file_name="Public/".$key1['id'].".".$exts;
									$objWriter->save($file_name);//保存生成  
									//$objWriter->save('php://output'); 
									$get_file.=$file_name.",";
				   }
				   
				   $files=explode(",",$get_file);
				   $this->create_zip($files, 'myfile.zip', true);
				   echo "/myfile.zip";
	}
*/

function getExcelValue($index)
{
    $index = (int)$index;if ($index <= 0) return; //输入检测
    $dimension = ceil(log(25 * $index + 26, 26)) - 1;  //算结果一共有几位，实际算的是位数减1，记住是26进制的位数
    $n = $index - 26 * (pow(26, $dimension- 1) - 1) / 25; //算结果在所在位数总数中排第几个
    $n--; //转化为索引
 
    return str_pad(
        str_replace(
            array_merge(range(0, 9), range('a', 'p')), 
            range('A', 'Z'), base_convert($n, 10, 26)
        ), $dimension, 'A', STR_PAD_LEFT
    ); //翻译加补齐
}

public function make_xls(){
					$task_id = I('param.task_id',0);
					if ( empty($task_id) ) {
						$this->error('请选择要操作的数据!');
					}
					$task_id=substr($task_id,0,strlen($task_id)-1);
					$map = " result_id in (".$task_id.")";

					
					
				    $filename="Public/xls/data_moban.xlsx";
					//导入PHPExcel类库，因为PHPExcel没有用命名空间，只能inport导入
					import("Org.Util.PHPExcel");
					//创建PHPExcel对象，注意，不能少了\
					$PHPExcel=new \PHPExcel();
					//如果excel文件后缀名为.xls，导入这个类
					$exts='xlsx';
					if($exts == 'xls'){
						import("Org.Util.PHPExcel.Reader.Excel5");
						$PHPReader=new \PHPExcel_Reader_Excel5();
					}else if($exts == 'xlsx'){
						import("Org.Util.PHPExcel.Reader.Excel2007");
						$PHPReader=new \PHPExcel_Reader_Excel2007();
					}
					  //载入文件
				  $PHPExcel=$PHPReader->load($filename);
					
				   //$task_id = I('get.task_id',0);
				   $all  = D("ft_result")->where($map)->order("check_date asc")->select();
				   $project_id=M("ft_result")->where($map)->getField("project_id");
				   $i = 2;
				   
				   //=======================================================表头===========================================
				   $list  = D("ft_form_field")->where(" project_id=$project_id")->order(" parent_id,sort_order desc ")->select();  
				   $coll_i=11;
				   $t=0;
				   foreach ($list as &$key) {
						$coll_i++;
						$t++;
					    $coll_name=$this->getExcelValue($coll_i);
						$coll_value=$key['field_id'].".".$key['field_title'];
					    $PHPExcel->getActiveSheet()->setCellValue($coll_name.$i,$coll_value);  //项目内容
				   }   
				   //=======================================================表头===========================================
				   
				   
				   foreach ($all as &$key1) {
					   $i++;
								  
								   $user_id=$key1['user_id'];
								   $store_id=$key1['store_id'];
								   $builddate=$key1['check_date'];
								   $uName=M("ft_user")->where(" user_id=".$user_id)->getField('uName');
								   
								   $store=M("ft_store")->where(" store_id=".$store_id)->find();  //门店信息
								   $store_name=$store['store_name'];
								   
								   $schedule=M("ft_schedule")->where(" id=".$key1['task_id'])->find();  //查找任务信息
								   
								   $PHPExcel->getActiveSheet()->setCellValue('A'.$i,$i);  
								   $PHPExcel->getActiveSheet()->setCellValue('B'.$i,$key1['task_id']);  
								   $PHPExcel->getActiveSheet()->setCellValue('C'.$i,$store['store_num']);   //网点编码 
								   $PHPExcel->getActiveSheet()->setCellValue('D'.$i,$store['zong_id']);   //大区
								   $PHPExcel->getActiveSheet()->setCellValue('E'.$i,$store['zong_store_name']);	 //总店简称
								   
								   
								   $progress_sum=M('ft_form_field')->where(" request=1 and project_id=$project_id")->count();	
								   
								   $progress_result=M('ft_form_field_value fffv')
												->join(" ft_form_field fff on fff.field_id=fffv.field_id")
												->where("fffv.user_id=".$key1['user_id']."  and fffv.task_id=".$key1['task_id']." and fff.request=1 and fff.project_id=$project_id")
												->count();  
									
								   $progress=round(($progress_result/$progress_sum)*100);	
								   
								   $PHPExcel->getActiveSheet()->setCellValue('F'.$i,$store['store_name']."(".$progress.")%");    //网点简称
								   $PHPExcel->getActiveSheet()->setCellValue('G'.$i,$store['store_address']);   //地址 
								   $PHPExcel->getActiveSheet()->setCellValue('H'.$i,$uName);   //检查员姓名 
								   $PHPExcel->getActiveSheet()->setCellValue('I'.$i,$schedule['builddate']);   //排班时间
								   $PHPExcel->getActiveSheet()->setCellValue('J'.$i,$key1['address']."(GPS定位真实:".$key1['address_true'].")");	 //上传地址
								   
								   if($key1['check_end_time']>0)
								   {
									   $check_end_time=date("Y-m-d H:i:s",$key1['check_end_time']);
								   }
								   $PHPExcel->getActiveSheet()->setCellValue('K'.$i,date("Y-m-d H:i:s",$key1['check_start_time'])."至".$check_end_time);	 //检查开始时间-检查结束时间
								   
								   //=======================================================后面自定义的内容出来===========================================
								   $coll_i=11;
								   $list  = D("ft_form_field")->where(" project_id=$project_id")->order(" parent_id,sort_order desc ")->select(); 
								   foreach ($list as &$key) {
									    $coll_i++;
										 $coll_name=$this->getExcelValue($coll_i);
										 
										$url='http://'.$_SERVER['SERVER_NAME'].':'.$_SERVER["SERVER_PORT"];
										$get_field_value=D("ft_form_field_value")->where(" task_id=".$key1['task_id']." and field_id=".$key['field_id']."")->getField("field_value");
										$old_name=D("ft_form_field_value")->where(" task_id=".$key1['task_id']." and field_id=".$key['field_id']."")->getField("old_name");
										
										//$hou_zui=end(explode('.',$get_field_value));
										//if($hou_zui=="jpg" or $hou_zui=="jpeg" or $hou_zui=="png" or$hou_zui=="gif"){
										$field_type=$key['field_type'];
										if($field_type=="file"){
												if($key['is_picture']){
													  $str_name="传图片文件-";
												}else{
													  $str_name="传图片名称-";
												}
												
												if($key['is_more']){
													  $str_name.="拍多张图片";
												}else{
													  $str_name.="拍单张图片";
												}
												if($key['is_picture'] && $get_field_value){
														$field_value=$str_name."\n".$url.$get_field_value."\n  设备上旧文件名:".$old_name;
												}elseif($old_name){
													    $field_value=$str_name."\n"."设备上旧文件名:".$old_name;
												}
										}else{
												$field_value=$get_field_value;
										}
										$PHPExcel->getActiveSheet()->setCellValue($coll_name.$i,$field_value);  //项目内容
										 
								  }
								  
								  //=======================================================后面自定义的内容出来===========================================
								  //header('pragma:public');
								  //header('Content-type:application/vnd.ms-excel;charset=utf-8;name="111.xls"');
								  //header("Content-Disposition:attachment;filename=$fileName.xls");//attachment新窗口打印inline本窗口打印
				    }
				   
				   // $files=explode(",",$get_file);
				   //$this->create_zip($files, 'myfile.zip', true);
				   $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007'); 
				   $file_name="Public/test.".$exts;
				   $objWriter->save($file_name);  
				   echo "/".$file_name;
}
	
	public function del(){
				$id = array_unique((array)I('post.id',0));
				if ( empty($id) ) {
					$this->error('请选择要操作的数据!');
				}
				$map = array('result_id' => array('in', $id) );
				
				
				$sql=D('ft_result')->where($map)->field('task_id')->buildSql();
				$list=D('ft_form_field_value')->where(" task_id in ".$sql)->select();
				if($list) {
					foreach($list as &$key){
						unlink(D('ft_form_field_value')->where(" old_name is not null and form_field_value_id=".$key['form_field_value_id'])->getField('field_value'));
					}
				}	
				
				D('ft_form_field_value')->where(" task_id in ".$sql)->delete();  //删除结果明细表
				D('ft_result')->where($map)->delete();  //删除结果表
				$this->success('删除成功');
		}
		
}
