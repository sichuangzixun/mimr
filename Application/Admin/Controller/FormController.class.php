<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: datahome改写 <datahome@qq.com>  2014-3-17
// +----------------------------------------------------------------------


/**
 * 后台用户控制器
 * @author  覃(qinshuxin@21cn.com)  2015-3-31
 */
class FormController extends AdminController {

    public function index(){
				$id = I('get.id',0);
				$keywords = I('get.keywords','');
				if($id)
				{
						$map=" parent_id=$id";
				}
				
				$page = I("get.p",1,intval);
		    	$limit = C('PAGE_LIMIT_NUM');
		    	if($page < 1){
		    		$page = 1;
		    	}
				$list  = D("ft_form_field")->where($map)->page("$page,$limit")->order(" sort_order desc ")->select();        
				$count = M('ft_form_field')->where($map)->count();
				$Page       = new \Org\Util\Page($count,$limit);// 实例化分页类 传入总记录数
				$show       = $Page->show();// 分页显示输出
				$this->assign("page",$show);
				
				
		 	
				
				if($list) {
					foreach($list as &$key){
						if($key['field_id']){
							$key['cat_name'] = D("ft_category")->where(" cat_id='".$key['parent_id']."'")->getField(" cat_name");
							
						}
					}
					$this->assign('_list', $list);
				}
				$this->display();
    }


   //===========================================================分类====================================================
   public function add(){
	  if(IS_POST){
			  $parent_id=$_POST[id];
			  $field_title=$_POST[field_title];
			  $field_content=$_POST[field_content];
			  $request=intval($_POST[request]);
			  $sort_order=intval($_POST[sort_order]);
			 
			  $field_type=$_POST["field_type"];
			  $field_name=$field_type.date("ymdHis",time());
			  
			  $nRemark=$_POST[nRemark];
			  $create_time=time();
			
			if(empty($field_title))
			{
				$this->error("字段标题不能为空！");
				exit;
			}
			    $is_picture=isset($_POST["is_picture"])?intval($_POST["is_picture"]):0;    //是否图片还是名称
				$is_more=isset($_POST["is_more"])?intval($_POST["is_more"]):0;             //单张图片还是多张图片
				

				
				$data['is_picture'] =  $is_picture;
				$data['is_more'] =  $is_more;
				
				
			$data['field_title'] =  $field_title;
			$data['field_content'] =  $field_content;
			$data['request'] =  $request;
			$data['sort_order'] =  $sort_order;
			$data['field_type'] =  $field_type;
			
			$data['field_name'] =  $field_name;
			$data['parent_id'] =  $parent_id;
			$data['nRemark'] =  $nRemark;
			$data['create_time'] =  $create_time;
			$data['count_limit'] =  $_POST['count_limit'];
			$data['project_id']=M("ft_category")->where(" cat_id=$parent_id")->getField("project_id");
			
		
            $res = M('ft_form_field')->add($data);
            if(0 < $res){
				$this->success('操作成功！',U('/Admin/Form/index','id='.$parent_id));
            } else {
                $this->error($res);  //$this->showRegError(
            }
        } else {
			    $this->display();
        }
		
	}

 
/*
 * 编辑功能
 */
    public function edit($id = 0){
			if(IS_POST){
    		$id = I('post.id',0);
    		  $field_title=$_POST[field_title];
			  $field_content=$_POST[field_content];
			  $request=intval($_POST[request]);
			  $sort_order=intval($_POST[sort_order]);
			 
			  $field_type=$_POST["field_type"];
			  $field_name=$field_type.date("ymdHis",time());
			  
			  $nRemark=$_POST[nRemark];
			
			
			if(empty($field_title))
			{
				$this->error("字段标题不能为空！");
				exit;
			}
			
			    $is_picture=isset($_POST["is_picture"])?intval($_POST["is_picture"]):0;    //是否图片还是名称
				$is_more=isset($_POST["is_more"])?intval($_POST["is_more"]):0;             //单张图片还是多张图片
				

				
				$data['is_picture'] =  $is_picture;
				$data['is_more'] =  $is_more;
				
				
			$data['field_title'] =  $field_title;
			$data['field_content'] =  $field_content;
			$data['request'] =  $request;
			$data['sort_order'] =  $sort_order;
			$data['field_type'] =  $field_type;
			
			$data['field_name'] =  $field_name;
			$data['nRemark'] =  $nRemark;
		    $data['count_limit'] =  $_POST['count_limit'];
			
			$parent_id=D('ft_form_field')->where(' field_id='.$id)->getField("parent_id");
			$res = M('ft_form_field')->where('field_id='.$id)->save($data);
			if(0 < $res){
    			$this->success('操作成功！',U('./Admin/Form/index','id='.$parent_id));
    		} else {
    			$this->error($res);  //$this->showRegError(
    		}
				
    	} else {
    		 $info = D('ft_form_field')->field(true)->find($id);
				 if(false === $info){
					$this->error('获取后台菜单信息错误');
				}
			   $this->assign('info', $info); 
			   $this->display();
    	}
    	
    }
    

		/**
		 * 删除分类
	    */
		public function del(){
			$id = array_unique((array)I('get.id',0));
			if ( empty($id) ) {
				$this->error('请选择要操作的数据!');
			}
			
			$map = array('field_id' => array('in', $id) );
			$parent_id=D('ft_form_field')->where($map)->getField(" parent_id");
			if(D('ft_form_field')->where($map)->delete()){
				$this->success('删除成功',U('/Admin/Form/index','id='.$parent_id));
			} else {
				$this->error('删除失败！');
			}
		}

}
