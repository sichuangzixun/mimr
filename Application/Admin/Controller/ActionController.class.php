<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: huajie <banhuajie@163.com>
// +----------------------------------------------------------------------

 

/**
 * 行为控制器
 * @author huajie <banhuajie@163.com>
 */
class ActionController extends AdminController {

    /**
     * 行为日志列表
     * @author huajie <banhuajie@163.com>
     */
    public function index(){
        
        $page = I("get.p",1,intval);
        $limit = C('PAGE_LIMIT_NUM');
        
        if($page < 1){
            $page = 1;
        }
        
        $pdata = I("get.");
        $starttime = $pdata['starttime'] ? trim($pdata['starttime']) : null;
        if($starttime && strtotime($starttime)){
            $map['create_time'] = array("EGT", strtotime($starttime." 00:00:00"));
        }
        $endtime = $pdata['endtime'] ? trim($pdata['endtime']) : null;
        if($endtime && strtotime($endtime)){
            $map['alog.create_time'] = array("ELT", strtotime($endtime." 23:59:59"));
        }
        $action_name = $pdata['action_name'] ? trim($pdata['action_name']) : null;
        if($action_name){
            $map['a.title'] = array("LIKE", "%$action_name%");
        }
        $user_name = $pdata['user_name'] ? trim($pdata['user_name']) : null;
        if($user_name){
            $map['m.nickname'] = array("LIKE", "%$user_name%");
        }
        
        //获取列表数据
        $model = 'ActionLog' ;  //模型名称        
        //查询过滤条件
        $map['alog.status']    =   array('gt', -1);  
        
        //使用后台排序
        //$list   =   $this->lists($model, $map);
        
        //使用前台排序
		$list  =  D($model)->alias("alog")
                ->join(" LEFT JOIN admin_action a on a.id=alog.action_id")
                ->join(" LEFT JOIN admin_member m on m.uid=alog.user_id")
                ->where($map)->page("$page,$limit")
                ->order(" alog.id DESC ")
                ->field("alog.*,a.title,m.nickname")
                ->select();
                
        $count = D($model)->alias("alog")
                ->join(" LEFT JOIN admin_action a on a.id=alog.action_id")
                ->join(" LEFT JOIN admin_member m on m.uid=alog.user_id")->where($map)->count();
        
        $Page       = new \Org\Util\Page($count,$limit);// 实例化分页类 传入总记录数
        $show       = $Page->show();// 分页显示输出
       
        $this->assign("page",$show);
        
        int_to_string($list);
//        foreach ($list as $key=>$value){
//            $model_id                  =   get_document_field($value['model'],"name","id");
//            $list[$key]['model_id']    =   $model_id ? $model_id : 0;
//        }
        $this->assign('_list', $list);
        $this->meta_title = '行为日志';
        $this->display("actionlog");
    }

    /**
     * 查看行为日志
     * @author huajie <banhuajie@163.com>
     */
    public function edit($id = 0){
        empty($id) && $this->error('参数错误！');

        $info = D('ActionLog')->field(true)->find($id);

        $this->assign('info', $info);
        $this->meta_title = '查看行为日志';
        $this->display();
    }

    /**
     * 删除日志
     * @param mixed $ids
     * @author huajie <banhuajie@163.com>
     */
    public function remove($ids = 0){
        empty($ids) && $this->error('参数错误！');
        if(is_array($ids)){
            $map['id'] = array('in', $ids);
        }elseif (is_numeric($ids)){
            $map['id'] = $ids;
        }
        $res = D('ActionLog')->where($map)->delete();
        if($res !== false){
            $this->success('删除成功！');
        }else {
            $this->error('删除失败！');
        }
    }

    /**
     * 清空日志
     */
    public function clear(){
        $res = D('ActionLog')->where('1=1')->delete();
        if($res !== false){
            $this->success('日志清空成功！');
        }else {
            $this->error('日志清空失败！');
        }
    }

}
