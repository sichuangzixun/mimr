<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: datahome改写 <datahome@qq.com>  2014-3-17
// +----------------------------------------------------------------------


/**
 * 后台用户控制器
 * @author  覃(qinshuxin@21cn.com)  2015-3-31
 */
class MemberController extends AdminController {

    public function index(){
				$cat_id = I('get.cat_id',0);
				$keywords = I('get.keywords','');
				if($cat_id)
				{
						$map=" cat_id=$cat_id";
				}
				if(!empty($keywords))
				{
						$map="  title Like '%".$keywords."%'";
				}
				//var_dump($map);
				//exit;
				$page = I("get.p",1,intval);
		    	$limit = C('PAGE_LIMIT_NUM');
		    	if($page < 1){
		    		$page = 1;
		    	}
				$list  = D("ft_user")->where($map)->page("$page,$limit")->order(" user_id DESC")->select();        
				$count = M('ft_user')->where($map)->count();
				$Page       = new \Org\Util\Page($count,$limit);// 实例化分页类 传入总记录数
				$show       = $Page->show();// 分页显示输出
				$this->assign("page",$show);
				
				
		 	
				if($list) {
					foreach($list as &$key){
						if($key['user_id']){
		$parent_id = D("ft_user")->where(" user_id=".$key['user_id'])->getField(" parent_id");
		$key['parent_name'] = D("ft_user")->where(" user_id=".$parent_id)->getField(" uName");
						}
					}
					$this->assign('_list', $list);
				}
				$this->display();
    }


   //===========================================================分类====================================================
   public function add(){
	   	  
	   	  if(IS_POST){
            $user_name=$_POST['user_name'];
			if(empty($user_name))
			{
				$this->error("登录手机号名称不能为空！");
				exit;
			}
			$count = M('ft_user')->where(" user_name='$user_name'")->count();
			if($count>0)
			{
				$this->error("有重复手机账号，请重新选择！");
			}
			$password =  $_POST['password'];
			$email =  $_POST['email'];
			$uName =  $_POST['uName'];
			$intType = isset($_POST["intType"])?intval($_POST["intType"]):0;
			
			if(empty($password))
			{
				$this->error("密码不能为空");
				exit;
			}
			
			if(empty($uName))
			{
				$this->error("名称不能为空!");
				exit;
			}
			
			$data['user_name'] =  $user_name;
			$data['password'] =  md5($password);
			$data['email'] =  $email;
			$data['uName'] =  $uName;
			$data['intType'] =  $intType;
			$data['reg_time'] = time();
            $parent_id = intval($_POST["parent_id"]);

			
            $res = M('ft_user')->add($data);
            if(0 < $res){
				$this->success('操作成功！',U('Admin/Member/index'));
            } else {
                $this->error($this->showRegError($res));
            }
        } else {
			 
			   $this->display();
        }
		
		
           
			
			
		   
    }

/*
 * 编辑功能
 */
    public function edit($id = 0){
    	
    	if(IS_POST){
    		$id = I('post.id',0);
			
    		$count = M('ft_user')->where(' user_id="$id"')->count();
			if($count)
			{
				$this->error("不存在此条记$count录");
			}
			$password =  $_POST['password'];
			$email =  $_POST['email'];
			$uName =  $_POST['uName'];
			$intType = isset($_POST["intType"])?intval($_POST["intType"]):0;
			$openid =  $_POST['openid'];
			$parent_id = intval($_POST["parent_id"]);
			$ublock = intval($_POST["ublock"]);
		
			
			if(!empty($password))
			{
				$data['password'] =  md5($password);
			}
			$data['email'] =  $email;
			$data['uName'] =  $uName;
			$data['intType'] =  $intType;
			$data['openid'] =  $openid;
			$data['parent_id'] =  $parent_id;
			$data['ublock'] =  $ublock;
		
    		
    		$res = M('ft_user')->where(' user_id='.$id)->save($data);
    		if(0 < $res){
    			$this->success('操作成功！',U('Admin/Member/index/'));
    		} else {
    			$this->error($res);
    		}
    	} else {
    		 $info = D('ft_user')->field(true)->find($id);
          	 if(false === $info){
                $this->error('获取后台菜单信息错误');
            }
           
            $this->assign('info', $info);
            $this->display();
    	}
    }
    

		/**
		 * 删除分类
	    */
		public function del(){
			$id = array_unique((array)I('get.id',0));
			//$cat_type = I('get.cat_type',0);
			if ( empty($id) ) {
				$this->error('请选择要操作的数据!');
			}
	
			$map = array('user_id' => array('in', $id) );
			if(D('ft_user')->where($map)->delete()){
				$this->success('删除成功');
			} else {
				$this->error('删除失败！');
			}
		}

}
