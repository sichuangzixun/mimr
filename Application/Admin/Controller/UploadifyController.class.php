<?php


class UploadifyController extends AdminController {
	
	public function upload(){
		
		
		 $this->display();
	}
	
	
	 public function file()
    {
        
        //加载文件上传，图片处理类
            import("@.ORG.UploadFile");
        //导入上传类
            $upload = new UploadFile();
        //设置上传文件大小
            $upload->maxSize=3292200;
        //设置上传文件类型
            $upload->allowExts=explode(',','jpg,gif,png,jpeg');
        //设置附件上传目录
            $upload->savePath='./Uploads/';
        //设置需要生成缩略图，仅对图像文件有效
            $upload->thumb = true;
        // 设置引用图片类库包路径
            $upload->imageClassPath ='@.ORG.Image';
        //设置需要生成缩略图的文件后缀
            $upload->thumbPrefix='m_,s_';  //生产2张缩略图
        //设置缩略图最大宽度
            $upload->thumbMaxWidth='400,100';
        //设置缩略图最大高度
            $upload->thumbMaxHeight='400,100';
        //设置上传文件规则
            $upload->saveRule='uniqid';
        //删除原图
            $upload->thumbRemoveOrigin=true;
          if( !$upload->upload() )
          {
              echo '0';
          }else{
              $info=$upload->getUploadFileInfo();
            $src='s_'.$info[0]['savename'];
            echo $src;
          }
    }
    public function  preview ()
    {
        $this->display();    
    }   
	
	
	}
?>