<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: datahome改写 <datahome@qq.com>  2014-3-17
// +----------------------------------------------------------------------


/**
 * 后台用户控制器
 * @author  覃(qinshuxin@21cn.com)  2015-3-31
 */
class StoreController extends AdminController {

    public function index(){
				$cat_id = I('get.cat_id',0);
				$keywords = I('get.keywords','');
				if($cat_id)
				{
						$map=" cat_id=$cat_id";
				}
				
				$page = I("get.p",1,intval);
		    	$limit = C('PAGE_LIMIT_NUM');
		    	if($page < 1){
		    		$page = 1;
		    	}
				$list  = D("ft_store")->where($map)->page("$page,$limit")->order(" store_id desc,province_id asc")->select();        
				$count = M('ft_store')->where($map)->count();
				$Page       = new \Org\Util\Page($count,$limit);// 实例化分页类 传入总记录数
				$show       = $Page->show();// 分页显示输出
				$this->assign("page",$show);
				
				
		 	
				
				if($list) {
					foreach($list as &$key){
						if($key['store_id']){
							$key['province'] = D("ft_province")->where(" provinceID='".$key['province_id']."'")->getField(" province");
							$key['city'] = D("ft_city")->where(" cityID='".$key['city_id']."'")->getField(" city");
						}
					}
					$this->assign('_list', $list);
				}
				$this->display();
    }


	//得到上传文件后续
	public function get_extension($file)
	{
		return end(explode('.', $file));
	}
   //===========================================================分类====================================================
   public function add(){
	  if(IS_POST){
		    $store_num=$_POST["store_num"];
			$zong_id=$_POST["zong_id"];
			$zong_store_name=$_POST["zong_store_name"];
			$store_name=$_POST["store_name"];
			$store_address=$_POST["store_address"];
			$do_office=$_POST["do_office"];
				
			$province_id=isset($_POST["province_id"])?intval($_POST["province_id"]):0;
			$city_id=isset($_POST["city_id"])?intval($_POST["city_id"]):0;
			
			if(empty($store_name))
			{
				$this->error("网点简称不能为空！");
				exit;
			}
			$data['store_num'] =  $store_num;
			$data['zong_id'] =  $zong_id;
			$data['zong_store_name'] =  $zong_store_name;
			$data['store_name'] =  $store_name;
			$data['store_full_name'] =  $_POST["store_full_name"];
			$data['store_address'] =  $store_address;
			$data['do_office'] =  $do_office;
			
			$data['province_id'] =  $province_id;
			$data['city_id'] =  $city_id;
			
			$data['create_time'] = time();
			
			
     
			
            $res = M('ft_store')->add($data);
            if(0 < $res){
				$this->success('操作成功！',U('/Admin/Store'));
            } else {
                $this->error($res);  //$this->showRegError(
            }
        } else {
			   $sheng= D("ft_province")->order("provinceID")->select();    
			   $this->assign('sheng', $sheng);   
			   $this->display();
        }
		
	}

 public function get_next($province_id = 0){
			 $province_id = I('get.province_id',0);
			 if( ! $province_id ) return false;
			 $res= D("ft_city")->where(" father='$province_id'")->order(" cityID  ")->select();    
			 //$str = "<select>";
			 foreach($res as $v)
			 {
			  	 $str.= "<option value=".$v['cityid'].">".$v['city']."</option>";
			 }
			// $str .= "</select>";  
			 echo $str ;
 
		}
/*
 * 编辑功能
 */
    public function edit($id = 0){
			if(IS_POST){
    		$id = I('post.id',0);
    		$store_num=$_POST["store_num"];
			$zong_id=$_POST["zong_id"];
			$zong_store_name=$_POST["zong_store_name"];
			$store_name=$_POST["store_name"];
			$store_address=$_POST["store_address"];
			$do_office=$_POST["do_office"];
				
			$province_id=$_POST["province_id"];
			$city_id=$_POST["city_id"];
			
			if(empty($store_name))
			{
				$this->error("网点简称不能为空！");
				exit;
			}
			$data['store_num'] =  $store_num;
			$data['zong_id'] =  $zong_id;
			$data['zong_store_name'] =  $zong_store_name;
			$data['store_name'] =  $store_name;
			$data['store_full_name'] =  $_POST["store_full_name"];
			$data['store_address'] =  $store_address;
			$data['do_office'] =  $do_office;
			
			$data['province_id'] =  $province_id;
			$data['city_id'] =  $city_id;
			
			$res = M('ft_store')->where('store_id='.$id)->save($data);
			if(0 < $res){
    			$this->success('操作成功！',U('./Admin/Store'));
    		} else {
    			$this->error($res);  //$this->showRegError(
    		}
				
    	} else {
    		 $info = D('ft_store')->field(true)->find($id);
				 if(false === $info){
					$this->error('获取后台菜单信息错误');
				}
			
			   $province_id=$info['province_id'];
			   $sheng= D("ft_province")->order("provinceID")->select();    
			   $city= D("ft_city")->where("father='$province_id'")->order("cityid")->select();    
			   $this->assign('sheng', $sheng);
			   $this->assign('city', $city);
			   $this->assign('info', $info); 
			   $this->display();
    	}
    	
    }
    

		/**
		 * 删除分类
	    */
		public function del(){
			$id = array_unique((array)I('get.id',0));
			if ( empty($id) ) {
				$this->error('请选择要操作的数据!');
			}
	
			$map = array('store_id' => array('in', $id) );
			if(D('ft_store')->where($map)->delete()){
				$this->success('删除成功');
			} else {
				$this->error('删除失败！');
			}
		}
		
		 public function upload(){
				   if(IS_POST){
						    header("Content-Type:text/html;charset=utf-8");
							$upload = new \Think\Upload();// 实例化上传类
							$upload->maxSize   =     3145728 ;// 设置附件上传大小
							$upload->exts      =     array('xls', 'xlsx');// 设置附件上传类
							
							$upload->rootPath  =     C(__UPLOAD__); // 设置附件上传根目录
							$upload->savePath  =      'xls/'; // 设置附件上传目录
							$upload->autoSub  = false;
							// 上传文件
							$info   =   $upload->uploadOne($_FILES['file1']);
							$filename = C(__UPLOAD__).'xls/'.$info['savename'];
							$exts = $info['ext'];
						    
							//print_r($info);exit;
							//$filename= C(__UPLOAD__).'xls/'."drugs_moban.xlsx";
							if(!$info) {// 上传错误提示错误信息
								  $this->error($upload->getError());
							  }else{// 上传成功
									  $this->goods_import($filename, $exts);
									 // $this->success('操作成功');
									 
							}

								
				   }
				   else
				   {
					 	 $this->display(); 
				   }
					   
		  }
		  
		  		   //导入数据方法
    protected function goods_import($filename, $exts='xls')
    {
        //导入PHPExcel类库，因为PHPExcel没有用命名空间，只能inport导入
        import("Org.Util.PHPExcel");
        //创建PHPExcel对象，注意，不能少了\
        $PHPExcel=new \PHPExcel();
        //如果excel文件后缀名为.xls，导入这个类
        if($exts == 'xls'){
            import("Org.Util.PHPExcel.Reader.Excel5");
            $PHPReader=new \PHPExcel_Reader_Excel5();
        }else if($exts == 'xlsx'){
            import("Org.Util.PHPExcel.Reader.Excel2007");
            $PHPReader=new \PHPExcel_Reader_Excel2007();
        }


        //载入文件
        $PHPExcel=$PHPReader->load($filename);
        //获取表中的第一个工作表，如果要获取第二个，把0改为1，依次类推
        $currentSheet=$PHPExcel->getSheet(0);
        //获取总列数
        $allColumn=$currentSheet->getHighestColumn();
        //获取总行数
        $allRow=$currentSheet->getHighestRow();
        //循环获取表中的数据，$currentRow表示当前行，从哪行开始读取数据，索引值从0开始
        for($currentRow=1;$currentRow<=$allRow;$currentRow++){
            //从哪列开始，A表示第一列
            for($currentColumn='A';$currentColumn<=$allColumn;$currentColumn++){
                //数据坐标
                $address=$currentColumn.$currentRow;
                //读取到的数据，保存到数组$arr中
                $data[$currentRow][$currentColumn]=$currentSheet->getCell($address)->getValue();
            }

        }
        	$this->save_import($data);
			unlink($filename);
			//var_dump($data);
    }



	 //保存导入数据
    public function save_import($data)
    {
        //print_r($data);exit;

        $Goods = M('ft_store');
        $add_time = time();
        foreach ($data as $k=>$v){
            if($k >= 2){
                $store_num=$v['B'];
                $info[$k-2]['store_num'] = $store_num;

                $zong_id=$v['C'];
                $info[$k-2]['zong_id'] = $zong_id;
				
				$zong_store_name=$v['D'];
                $info[$k-2]['zong_store_name'] = $zong_store_name;  
				
				$store_name=$v['E'];
                $info[$k-2]['store_name'] = $store_name; 
				
				$store_full_name=$v['J'];   //全称
                $info[$k-2]['store_full_name'] = $store_full_name; 
				
				
				$store_address=$v['K'];    //地址
                $info[$k-2]['store_address'] = $store_address; 
				
				$province_name=$v['F'];
				$info[$k-2]['province_id'] = D("ft_province")->where(" province like '%".$province_name."%'")->getField("provinceID");  
								
				$city_name=$v['G'];
				$info[$k-2]['city_id'] =D("ft_city")->where(" city like '%".$city_name."%'")->getField("cityID");  
				
				$info[$k-2]['create_time'] =time();
				$count=$Goods->where(" store_name='$store_name'")->count();
				if(intval($count)>0)
				{
						continue;
				}
				$result = $Goods->add($info[$k-2]);
				

            }

        }

        
			echo "<script>window.parent.location.reload();</script>";
		/*	
		if($result){
            //$this->success('产品导入成功');
        }else{
            $this->error('产品导入失败');
        }
        //print_r($info);
		*/

    }

}
