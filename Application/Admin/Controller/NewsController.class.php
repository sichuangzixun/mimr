<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: datahome改写 <datahome@qq.com>  2014-3-17
// +----------------------------------------------------------------------


/**
 * 后台用户控制器
 * @author  覃(qinshuxin@21cn.com)  2015-3-31
 */
class NewsController extends AdminController {

    public function index(){
				$cat_id = I('get.cat_id',0);
				$keywords = I('get.keywords','');
				if($cat_id)
				{
						$map=" cat_id=$cat_id";
				}
				if(!empty($keywords))
				{
						$map="  title Like '%".$keywords."%'";
				}
				//var_dump($map);
				//exit;
				$page = I("get.p",1,intval);
		    	$limit = C('PAGE_LIMIT_NUM');
		    	if($page < 1){
		    		$page = 1;
		    	}
				$list  = D("ft_news")->where($map)->page("$page,$limit")->order(" sort_order DESC,id desc ")->select();        
				$count = M('ft_news')->where($map)->count();
				$Page       = new \Org\Util\Page($count,$limit);// 实例化分页类 传入总记录数
				$show       = $Page->show();// 分页显示输出
				$this->assign("page",$show);
				
				
		 		//$count = D("website_category")->where($map)->count();
				//$this->assign('count', $count);
				
				
				$this->assign('_list', $list);
				
				$this->display();
    }


	//得到上传文件后续
	public function get_extension($file)
	{
		return end(explode('.', $file));
	}
   //===========================================================分类====================================================
   public function add(){
	  if(IS_POST){
			
			$title=$_POST["title"];
			$content=$_POST["content"];
			$is_show=isset($_POST["is_show"])?intval($_POST["is_show"]):0;
			$tags=$_POST["tags"];
			$sort_order=isset($_POST["sort_order"])?intval($_POST["sort_order"]):0;
			
			$cat_id=isset($_POST["cat_id"])?intval($_POST["cat_id"]):0;
			$seo_title=$_POST["seo_title"];
			$seo_description=$_POST["seo_description"];
			if(empty($title))
			{
				$this->error("标题不能为空！");
				exit;
			}
			$data['title'] =  $title;
			$data['content'] =  $content;
			$data['is_show'] =  $is_show;
			$data['tags'] =  $tags;
			$data['sort_order'] =  $sort_order;
			
			$data['cat_id'] =  $cat_id;
			$data['seo_title'] =  $seo_title;
			$data['seo_description'] =  $seo_description;
			$data['create_time'] = date("Y-m-d H:i:s",time());
			$data['nUpDate'] =  date("Y-m-d H:i:s",time());
			
          	$data['pImage'] =  $pImage;
			
            $res = M('ft_news')->add($data);
            if(0 < $res){
				$this->success('操作成功！',U('/Admin/News'));
            } else {
                $this->error($this->showRegError($res));
            }
        } else {
			 
			   $this->display();
        }
		
		
           
			
			
		   
    }

/*
 * 编辑功能
 */
    public function edit($id = 0){
			if(IS_POST){
    		$id = I('post.id',0);
    		$title=$_POST["title"];
			$content=$_POST["content"];
			$is_show=isset($_POST["is_show"])?intval($_POST["is_show"]):0;
			$tags=$_POST["tags"];
			$sort_order=isset($_POST["sort_order"])?intval($_POST["sort_order"]):0;
			
			$cat_id=isset($_POST["cat_id"])?intval($_POST["cat_id"]):0;
			$seo_title=$_POST["seo_title"];
			$seo_description=$_POST["seo_description"];
			if(empty($title))
			{
				$this->error("标题不能为空！");
				exit;
			}
			
			
		
			
			$data['title'] =  $title;
			$data['content'] =  $content;
			$data['is_show'] =  $is_show;
			$data['tags'] =  $tags;
			$data['sort_order'] =  $sort_order;
			
			$data['cat_id'] =  $cat_id;
			$data['seo_title'] =  $seo_title;
			$data['seo_description'] =  $seo_description;
			$data['nUpDate'] =  date("Y-m-d H:i:s",time());
			$data['pImage'] =  $pImage;
			
    		//$this->error($id);
    		$res = M('ft_news')->where('id='.$id)->save($data);
    		if(0 < $res){
    			$this->success('操作成功！',U('./Admin/News'));
    		} else {
    			$this->error($this->showRegError($res));
    		}
    	} else {
    		 $info = D('ft_news')->field(true)->find($id);
          	 if(false === $info){
                $this->error('获取后台菜单信息错误');
            }
			   $this->assign('info', $info); 
			   $this->display();
    	}
    	
    }
    

		/**
		 * 删除分类
	    */
		public function del(){
			$id = array_unique((array)I('get.id',0));
			if ( empty($id) ) {
				$this->error('请选择要操作的数据!');
			}
	
			$map = array('id' => array('in', $id) );
			unlink(C(__UPLOAD__).M('ft_news')->where($map)->getField("pImage"));  
			if(D('ft_news')->where($map)->delete()){
				$this->success('删除成功');
			} else {
				$this->error('删除失败！');
			}
		}


		 public function upload(){
				   if(IS_POST){
						    header("Content-Type:text/html;charset=utf-8");
							$upload = new \Think\Upload();// 实例化上传类
							$upload->maxSize   =     3145728 ;// 设置附件上传大小
							$upload->exts      =     array('xls', 'xlsx');// 设置附件上传类
							
							$upload->rootPath  =     C(__UPLOAD__); // 设置附件上传根目录
							$upload->savePath  =      'xls/'; // 设置附件上传目录
							$upload->autoSub  = false;
							// 上传文件
							$info   =   $upload->uploadOne($_FILES['file1']);
							$filename = C(__UPLOAD__).'xls/'.$info['savename'];
							$exts = $info['ext'];
						    
							//print_r($info);exit;
							//$filename= C(__UPLOAD__).'xls/'."drugs_moban.xlsx";
							if(!$info) {// 上传错误提示错误信息
								  $this->error($upload->getError());
							  }else{// 上传成功
									  $this->goods_import($filename, $exts);
									 // $this->success('操作成功');
									 
							}

								
				   }
				   else
				   {
					 	 $this->display(); 
				   }
					   
		  }
		  
		  		   //导入数据方法
    protected function goods_import($filename, $exts='xls')
    {
        //导入PHPExcel类库，因为PHPExcel没有用命名空间，只能inport导入
        import("Org.Util.PHPExcel");
        //创建PHPExcel对象，注意，不能少了\
        $PHPExcel=new \PHPExcel();
        //如果excel文件后缀名为.xls，导入这个类
        if($exts == 'xls'){
            import("Org.Util.PHPExcel.Reader.Excel5");
            $PHPReader=new \PHPExcel_Reader_Excel5();
        }else if($exts == 'xlsx'){
            import("Org.Util.PHPExcel.Reader.Excel2007");
            $PHPReader=new \PHPExcel_Reader_Excel2007();
        }


        //载入文件
        $PHPExcel=$PHPReader->load($filename);
        //获取表中的第一个工作表，如果要获取第二个，把0改为1，依次类推
        $currentSheet=$PHPExcel->getSheet(0);
        //获取总列数
        $allColumn=$currentSheet->getHighestColumn();
        //获取总行数
        $allRow=$currentSheet->getHighestRow();
        //循环获取表中的数据，$currentRow表示当前行，从哪行开始读取数据，索引值从0开始
        for($currentRow=1;$currentRow<=$allRow;$currentRow++){
            //从哪列开始，A表示第一列
            for($currentColumn='A';$currentColumn<=$allColumn;$currentColumn++){
                //数据坐标
                $address=$currentColumn.$currentRow;
                //读取到的数据，保存到数组$arr中
                $data[$currentRow][$currentColumn]=$currentSheet->getCell($address)->getValue();
            }

        }
        	$this->save_import($data);
			unlink($filename);
			//var_dump($data);
    }



	 //保存导入数据
    public function save_import($data)
    {
        //print_r($data);exit;

        $Goods = M('ft_news');
        $add_time = time();
        foreach ($data as $k=>$v){
            if($k >= 2){
                $title=$v['A'];
                $info[$k-2]['title'] = $title;

                $content=$v['B'];
                $info[$k-2]['content'] = $content;
				
				$sort_order=$v['C'];
                $info[$k-2]['sort_order'] = $sort_order;  //任务时间
				
				$info[$k-2]['create_time'] = date("Y-m-d H:i:s",time());
				

                $result = $Goods->add($info[$k-2]);

            }

        }

        if($result){
			echo "<script>window.parent.location.reload();</script>";
            //$this->success('产品导入成功');
        }else{
            $this->error('产品导入失败');
        }
        //print_r($info);

    }
}
