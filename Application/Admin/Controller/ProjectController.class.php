<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: datahome改写 <datahome@qq.com>  2014-3-17
// +----------------------------------------------------------------------


/**
 * 后台用户控制器
 * @author  覃(qinshuxin@21cn.com)  2015-3-31
 */
class ProjectController extends AdminController {

    public function index(){
				$type_id=I("param.type_id",0,intval);  
				$map=" id>0";
				if($type_id){
						$map.=" and type_id=$$type_id";
				}
				$page = I("get.p",1,intval);
		    	$limit = C('PAGE_LIMIT_NUM');
		    	if($page < 1){
		    		$page = 1;
		    	}
				$list  = D("ft_project")->where($map)->page("$page,$limit")->order(" id desc")->select();  
				foreach($list as &$row){
					$row['cat_count']=M("ft_category")->where(" parent_id=0 and project_id=".$row['id'])->count();
				}
				$this->assign('_list', $list);
			
				$count = M('ft_project')->where($map)->count();
				$Page       = new \Org\Util\Page($count,$limit);// 实例化分页类 传入总记录数
				$sum = ceil($count/$limit);
				
				$show       = $Page->show();// 分页显示输出
				$this->assign("page",$show);  
				$this->display();
    }


	
   //===========================================================分类====================================================
public function add(){
	   if(IS_POST){	  
							$name=I("param.name",0);
							$is_show=I("param.is_show",0,intval);
							if(empty($name)){
								$this->error("名称不能为空！");
								exit;
							}

							
							$data['name']=$name;
							$data['is_show']=$is_show;
							$data['create_time']=time();
				
										
							$res = M('ft_project')->add($data);
							if(0 < $res){
									$this->success("添加成功！",U('Project/index'));
							} else {
									$this->error($res);
							}
		 } else {
					       $this->display();
        }
		
		
}

  
		


/*
 * 编辑功能
 */
    public function edit($id = 0){
    	
    	if(IS_POST){
					$id = I('post.id',0);
					
					$name=I("param.name",0);
					$is_show=I("param.is_show",0,intval);
					if(empty($name)){
						$this->error("名称不能为空！");
						exit;
					}
					$data['name']=$name;
					$data['is_show']=$is_show;
					
					$res =D('ft_project')->where('id='.$id)->save($data);
					if(0 < $res){
							$this->success("修改成功！",U('Project/index'));
					} else {
							$this->error($res);
					}
		} else {
					 $info = D('ft_project')->field(true)->find($id);
					 if(false === $info){
						$this->error('获取后台菜单信息错误');
					 }
					 $this->assign('info', $info);
					 $this->display("add");
    	}
    }
    
 	
		  
	/**
	 * 删除分类
    */
public function del(){
		$id = array_unique((array)I('get.id',0));
		if ( empty($id) ) {
			$this->error('请选择要操作的数据!');
		}
		
		
		$cat_count=M("ft_category")->where(" parent_id=0 and project_id=".I("param.id"))->count();
		if($cat_count){
			$this->error('你下级有问卷，请先删除下级问题!');
		}
		$map = array('id' => array('in', $id) );
		if(D('ft_project')->where($map)->delete()){
			$this->success('删除成功');
		} else {
			$this->error('删除失败！');
		}
	}
}