<?php
	include("weixin/wx_config.php");
	include("weixin/common.php");
	
			
	$REDIRECT_URI='http://m.mimr.ipanyu.cn/get_userinfo.php';
    $scope='snsapi_base'; //$scope='snsapi_userinfo';//需要授权
    $url='https://open.weixin.qq.com/connect/oauth2/authorize?appid='.APPID.'&redirect_uri='.urlencode($REDIRECT_URI).'&response_type=code&scope='.$scope.'&state='.$state.'#wechat_redirect';
    
	$code=$_GET['code'];
	if(!isset($code))
	{
    		header("Location:".$url);
			exit;
	}
	
	
	
    $get_token_url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid='.APPID.'&secret='.APPSECRET.'&code='.$code.'&grant_type=authorization_code';
    $res = http_request_json($get_token_url);  
    $json_obj = json_decode($res,true);  
      
    //根据openid和access_token查询用户信息  
    $access_token = $json_obj['access_token'];  
    $openid = $json_obj['openid'];  
    $get_user_info_url = 'https://api.weixin.qq.com/sns/userinfo?access_token='.$access_token.'&openid='.$openid.'&lang=zh_CN';  
      
    $res = http_request_json($get_user_info_url);  
      
    //解析json  
    $user_obj = json_decode($res,true); 
	$nickname = $user_obj['nickname'];   
	
	$nickname=urlencode($nickname);
	$url="http://58.67.144.65:8088/Home/Binding/index/";
	$url.="?openid=".$openid."&nickname=".$nickname;
	header("Location:".$url);