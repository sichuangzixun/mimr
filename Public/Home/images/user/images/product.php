<?php
include("../admin/global.php");
//include("../admin/check.php");
$id=intval($_REQUEST[id]);
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=device-width,minimum-scale=1.0,maximum-scale=1.0" name="viewport" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta content="telephone=no" name="format-detection" />
<link rel="apple-touch-icon-precomposed" href="/qunar-touch.png"/>
<title>产品展示 - <?=getTableName("ky_web","company_name","nId",1)?></title>
<link rel="stylesheet" href="css/common@0798c38290a76ca861ea29ecf07d661a.css?v1"  type='text/css' />
<link rel="stylesheet" href="css/group@f5d6a881c1cde33ef6116f6560ee91d9.css?v2"  type='text/css' />
<link href="css/touchindex@6c36ec98de786b057fdb5b8ffe169409.css?v1" rel="stylesheet" type="text/css" />
<style tyle="text/css">
.qn_main {
	width:320px;
	margin:0 auto;
}
.qn_header .right {
	text-align:center;
	line-height:16px;
}
.qn_header .right .icon_header_icon {
	margin-top:4px;
}
.qn_header .right em {
	display:block;
	font-size:12px;
	line-height:12px;
}
.qn_header .place {
	right:42px;
}
.qn_header .title a {
	color:#fff;
	font-size:16px;
}
.qn_header .qn_select {
	-webkit-appearance: none;
	color:#fff;
	background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAAGCAYAAAAVMmT4AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAABlSURBVChTY/j3718LED8G4pt48FOg3EQGICEExLv/4wFA+c1AaR4GEAAy+IEC+7GpB4pvhysEqwYCoKAoEJ9A1gDkHwLZDFODQgMlZID4IlTDGSAtgVUhTBCoQAOoYS2QVkJXCABRaamEEuV7bQAAAABJRU5ErkJggg==) center right no-repeat;
}
.qn_header .qn_select option {
	color:#000;
}
.qn_header .right {
	width:48px;
}
.qn_bt .count .superRedPacket {
	background-color:red;
	color:#fff;
	padding:2px;
	border-radius:2px;
}
input::-webkit-search-decoration, input::-webkit-search-cancel-button {
display: none;
}
.qn_group_list .location {
	bottom:-5px;
}
.Qt_f14 {
	font-size:14px;
}
.Qt_spec {
	color: #ff3300;
}
.Qt_textb {
	color:#999;
}
.qn_group_list .price {
	height:22px;
	line-height:22px;
	margin-top:4px;
}
.qn_group_list .groupprice {
	font-size:20px;
	margin-right:2px;
}
.qn_group_list .oldprice, .qn_group_list .yhprice {
	font-size:14px;
}
.qn_group_list .dis {
	font-size:12px;
	background:#ff9933;
	color:#fff;
	padding:0 3px;
	margin:1px 8px;
	vertical-align:top;
}
.qn_group_list .yhprice {
	margin-left:2px;
}
#newp_sort { width:100%; overflow:hidden; border-top:1px solid #eee; border-bottom:1px solid #eee; background:#f7f7f7; }
#newp_sort * { margin:0; padding:0; list-style:none; }
#newp_sort ul { float:left; width:100%; overflow:hidden; margin:0 -5px 0 0; }
#newp_sort li { float:left; width:25%; text-align:center; font:14px/2 'Microsoft Yahei', Arial, SimHei, Verdana, sans-serif; }
#newp_sort a { border-bottom:2px solid #f7f7f7; padding:0 0 1px; }
#newp_sort a:link,#newp_sort a:visited { color:#666; }
#newp_sort a:hover { color:#25a4bb; text-decoration:none; border-bottom-color:#25a4bb; }
</style>
</head>
<body>
<div class="qn_main">
<div class="qn_pages">
<div id="groupList">
  <div class="qn_header">
    <div class="back"><a href="./">后退</a></div>
   <div class="title">
   <!--
                <select class="qn_select" id="first_tag">
                                        <option value="酒店"  >
                    &nbsp;&nbsp;&nbsp;&nbsp;酒店
                    </option>
                                        <option value="门票" selected="selected" >
                    &nbsp;&nbsp;&nbsp;&nbsp;门票
                    </option>
                                        <option value="国内游"  >
                    &nbsp;&nbsp;国内游
                    </option>
                                        <option value="周边游"  >
                    &nbsp;&nbsp;周边游
                    </option>
                                    </select>-->
            </div>
            
            
     </div>
     <!--搜索-->
     <form>
        <div class="seach_wrap">
            <div class="clrfix seach_box">
                <div class="seach_box_swap">
                    <input type="text" id="search-query" placeholder="输入目的关键词" class="inp_text" name="keyWords"><a id="search-commit" class="btn" href="javascript:document.forms[0].submit();">搜索</a>
                </div>
            </div>
            <div style="display: none;" id="suggest-list" class="js_package_suggest_container">
                <ul></ul>
            </div>
        </div>
       </form> 
  <div id="newp_sort">
    <ul>
      
	 
	  <?php
	  $i=0;
	  $res=mysql_query("select * from ky_category Where  parent_id=0 And channelID=50 order by sort_order desc");//
	  while($row = mysql_fetch_array($res))
	  {
		  	 $i++;
	  ?>
     		 <li><span> <a href="product.php?id=<?=$row[cat_id]?>"><?=$row[cat_name]?></a> </span> </li>
	  <?php
	  		//if($i%3==0) echo"  </ul> <ul>";
	  }
	  ?>
     
       <li><span><a href="product.php">全部</a></span> <span class="qn_triangle_right"></span> </li>
     
    </ul>
  </div>
  
  <div class="qn_list qn_group_list">
    <ul>
<?php

		$allsql=" Where block=1";


		
		if(!empty($_GET['keyWords']) and $_GET['keyWords']!="输入关键字")
		{
			$allsql=$allsql." And name Like '%".$_GET['keyWords']."%' or nRemark Like '%".$_GET['keyWords']."%'  ";
		}
		
		if($id)
		{
				$allsql.=" and (cat_id=$id or cat_id in (select cat_id from ky_category where parent_id=$id))";
		}
	
		
   		$page_size=30;   
        $res=mysql_query("select count(*) from ky_product ".$allsql ,$conn);
		$myrow = mysql_fetch_array($res);
		$numrows=$myrow[0];
		//计算总页数
		
		
		$pages=intval($numrows/$page_size);
		
		
		if ($numrows%$page_size)
		{
			$pages++;
		}
		//判断页数设置与否，如无则定义为首页
		if (!isset($page))
		$page=1;
		
		//计算记录偏移量
		$offset=$pagesize*($page-1);
		$pagesize=$page_size;
		//总条目数    
		$nums=$numrows;    
		//每次显示的页数    
		$sub_pages=10;    
		//得到当前是第几页    
		$pageCurrent=$_GET["p"];    

        if(!$pageCurrent) $pageCurrent=1;    
	    $offset=($pageCurrent-1)*$pagesize;  //起始页     
   		$urlSql="?nType=$nType&nType1=$nType1&keywords=".$_GET['keywords']."";
   
$sql1=mysql_query("select * from ky_product $allsql order by sort_order Desc limit $offset,$pagesize",$conn);
$num=$numrows;
?>

<?php 
while($row=mysql_fetch_array($sql1))
	 {
			if(!empty($row[pImage]))  
			{
						$getimage=explode("|",$row[pImage]);
						if(count($getimage)>1)  //说明有 "|"
						{ 
							$pImage=$getimage[0];
						}
						else
						{
							$pImage=$row[pImage];
						}
			}			 
				 
?> <a href="product_view.php?id=<?=$row[pid]?>">
      <li class="qn_bt" data-seq="222900">
        <div class="img"><img class="lazy-load" src="<?=QSX_SYSTEM_PATH.$pImage?>" onerror="images/nopic.gif"></div>
        <div class="info qn_arrow_grey r">
          <div class="title"><?=$row[name]?></div>
		  <div class="desc">
		  	<?=msubstr(strip_tags($row[nRemark]),0,75,'utf-8',true)?>
		  </div>
		 
		  
		  
<div class="Qt_spec price">
		<?php
		if ($row[good]==1) { echo '<div class="count" style="display:inline;"><span class="superRedPacket">推荐</span></div>'; }
		?>
		<?php
        if($row[market_price]>0)
		{
		?>
         <em>&yen;</em> <em class="groupprice"><?=$row[market_price]?> </em>  
        <?php }?>
        <?=getTableName("ky_category","cat_name","cat_id",$row[cat_id])?>
         <del class="Qt_textb oldprice"></del> 
</div>

        </div>
      </li></a>
<?php
			 }
?>      
      <!--
      <li class="qn_bt" data-seq="450356">
        <div class="img"> <img class="lazy-load" src="http://img1.qunarzz.com/tuan/team2/1409/16/c3a231d19761440c213a9cdb.jpg_r_113x72_80594a3c.jpg">
          <p class="img_des">已售出1430份</p>
        </div>
        <div class="info qn_arrow_grey r">
          <div class="title">仅258元，北京美豪富邦酒店团购中！</div>
          <div class="count"> <span class="superRedPacket">超级红包</span> </div>
          <div class="Qt_spec price"> <em>&yen;</em> <em class="groupprice">258 </em> -<em class="yhprice">12</em> <span class="dis">立减</span> <del class="Qt_textb oldprice">&yen;538</del> </div>
        </div>
      </li>
      -->
    </ul>
    <!--
    <div class="bottom">
      <div class="more">点击查看更多</div>
      <div class="goTop"><span>回到顶部</span></div>
    </div>
    -->
    <div class="ad"> </div>
  </div>
</div>
<?php include("foot.php");?>
</body>
</html>
