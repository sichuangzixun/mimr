/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/*服务白名单*/
function openSever(obj){
    var hospital_id = $(obj).parents("tr").find("td").eq(0).text();
    if(!hospital_id){
        updateAlert ('参数错误', 'alert-error');
        setTimeout(function(){$('#top-alert').removeClass("block"); },1000);
        return false;
    }
    $.post(getBoxUrl,{his_id:hospital_id},function(data){
        $("#add_service").html(data);
    });

    var mySever = new $.thinkbox($("#add_service"),{'width':600,'height':400,'title':'标题','tools':true,'buttons':{'close':['关闭']},afterShow:function(){
        var inputItem=$("#keyContent");
        var autonode=$(".search_results");
        var autonodeUl=$(".search_results ul");
        inputItem.attr("hospital_id",hospital_id);
        var time =  null;
        pageFun.init();
        $('.h_list').css({height:300,'margin':'10px 0 0'}).click(function(){
            autonode.hide();
        })
        $('.h_step').css({height:400})

        //设置提示框隐藏
        autonode.hide();
        var timer= null;
        $("#keyContent").keyup(function(event){
            var myEvent=event||window.event;
            var keyCode=myEvent.keyCode;
            clearTimeout(timer);
            var word=$("#keyContent").val();

            if(!word){ autonode.hide();return}
            if(keyCode ==38 || keyCode ==40 || keyCode == 13){
                var indexObj =$(".ul_search_results li.active"),
                    len =$(".ul_search_results li").length,
                    text ='',
                    liObj =$(".ul_search_results li");
                switch (keyCode){
                    case 38://向上
                        if(autonode.css('display') == 'none') return;
                        var index = indexObj.index()-1;
                        index = (index || index<0)?index:0;
                        liObj.removeClass('active');
                        text = liObj.eq(index).addClass('active').find('.word').html();
                        if(index == -1){
                            scrollToOBJ($('.search_results')[0].scrollHeight);
                        }else{
                            scrollToOBJ(index);
                        }
                        $("#keyContent").val(text);
                        var event = $(".ul_search_results li.active .opt").attr('onclick');
                        $('.h_step .searchBtn').attr('onclick',event)
                        break;

                    case 40://向下
                        if(autonode.css('display') == 'none') return;
                        var index = indexObj.index()+1;
                        if(!index){ index = 0 } else if (index >= len) { index = 0; }
                        liObj.removeClass('active');
                        text = liObj.eq(index).addClass('active').find('.word').html();
                        var scrollY = liObj.eq(index).position().top;
                        if(index == 0){
                            scrollToOBJ(0);
                        }
                        if(scrollY>287){
                            scrollToOBJ(index);
                        }
                        $("#keyContent").val(text);
                        var event = $(".ul_search_results li.active .opt").attr('onclick');
                        $('.h_step .searchBtn').attr('onclick',event)
                        break;

                    case 13://回车
                        if(autonode.css('display') == 'none') return;
                        $("#keyContent").val(text);
                        $(".ul_search_results li.active .opt").trigger('click');
                        setTimeout(function(){$('.ul_search_results').html()},350)
                        break;

                }
            }else{
                $('.h_step .searchBtn').attr('onclick','addRowData()')
                timer = setTimeout(function(){
                    getData(word);
                },500)
            }

        });

        //滚动条
        function scrollToOBJ(n){
            n = n*40;
            $('.search_results')[0].scrollTop=n;
        }
        function getData(word){
            clearTimeout(timer);
            $.ajax({
                url:PostDataUrl,
                dataType:"JSON",
                type:"POST",
                data:{hospital_id:hospital_id,word:word},
                success:function(res){
                    var html = "";
                    if(res.code == 40000){
                        for(var i in res.data){
                            html += '<li>';
                            if(res.data[i].white_list_status == 1){
                                html += '<div class="opt">已添加</div>';
                            }else{
                                html += '<div class="opt" onclick="addRowData(this,'+res.data[i].acc_id+',\''+res.data[i].acc_realname+'\');"><i class="icon_add"></i>点击添加</div>';
                            }
                            html += '<div class="word">'+res.data[i].acc_realname+'</div>';
                            html += '</li>';
                        }
                        autonode.find("ul").html(html);
                    }else{
                        html += '<li style="text-align: center;">'+res.msg+'</li>';
                        autonode.find("ul").html(html);
                    }
                    autonode.show();
                }
            });
        }
    }

    });
}

window.pageFun =function(){};
pageFun.goToPage =function(n){
    if( typeof n == "number"){
        if(n>0 && n<=this.totalPage){
            this.currentPage = n;
            if(this.currentPage<1){return;}
            this.showPage(this.currentPage)
        }else{
            console.log(n+'页无效。')
        }
    }else if(n == '-1'){
        if(this.currentPage-1<0){return;}
        this.currentPage --;
        this.showPage(this.currentPage)
    }else if(n == '+1'){
        if(this.currentPage>=this.totalPage-1){return;}
        this.currentPage ++;
        this.showPage(this.currentPage)
    }
}
pageFun.showPage =function(n){
    this.tr.hide();
    var star = n*this.page;
    var end = star+this.page;
    end = end>this.len?this.len:end;
    this.tr.slice(star, end).show();
    $('#my_currentPage').html(this.currentPage+1);

}

pageFun.init = function(){
    console.log('初始化')
    this.tr = $('.h_list tbody tr');
    this.obj = $('.h_list tbody');
    this.len = this.tr.length;
    this.page = 7;
    this.starIndex = 0;
    this.endIndex = this.page+1;
    this.currentPage = 0;
    this.totalPage = parseInt(this.len / this.page);
    this.offsetLen = this.len % this.page;
    if(this.offsetLen>0){
        this.totalPage++
    }
    var PageNav = $('#my_pageNav');
    $('#my_prev').attr('onclick','pageFun.goToPage("-1")')
    $('#my_next').attr('onclick','pageFun.goToPage("+1")')
    $('#my_currentPage').html(this.currentPage+1);
    $('#my_totalPage').html(this.totalPage);
    pageFun.showPage(0)

}
